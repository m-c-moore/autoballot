# Contributing

We'd love to have your help! We want to make contributing to this project as easy as possible, whether it's:

- Submitting a fix, new feature or performance improvement;
- Adding unit, integration or benchmark tests;
- Improving the documentation;
- Discussing the current state of the code;
- Proposing new features or identifying bugs.

One of the reasons for opening this codebase to contributors outside the JCR committee was to give students the opportunity to be part of a public project. As such, **we welcome contributions from anyone regardless of experience**, and we are very happy to support you to help get your changes merged!

---

- [Issues](#issues)
  - [Creating issues](#creating-issues)
  - [Solving issues](#solving-issues)
- [Making changes](#making-changes)
  - [Recommended workflow](#recommended-workflow)
  - [Accepting merge requests](#accepting-merge-requests)
- [Code quality checks](#code-quality-checks)
  - [Pre-commit](#pre-commit)
  - [CI pipelines](#ci-pipelines)
- [Privacy](#privacy)
- [License](#license)

## Issues

We use [issues](https://gitlab.com/m-c-moore/autoballot/-/issues) to track to-dos, suggestions and bugs.

### Creating issues

If you have an idea, spot a bug or just want to discuss current state of the code, first please check whether an issue already exists. If it does not, feel free to [create a new one](https://gitlab.com/m-c-moore/autoballot/-/issues/new?issue).

### Solving issues

If you're wanting to contribute code to this project, have a look at the [current issues](https://gitlab.com/m-c-moore/autoballot/-/issues) to see if there are any that are of interest to you. You don't need to solve the whole issue in one go - feel free to tackle part of one, just make it clear in a comment what you're going to do!

Issues are weighted by importance, with 1 being nice-to-haves and 3 being issues that need fixing before the next release. Issues weighted 0 are for non-essential but potentially significant changes to the program's structure.

## Making changes

Changes to the code are proposed through [merge requests](https://gitlab.com/m-c-moore/autoballot/-/merge_requests) (aka pull requests).

### Recommended workflow

1. Pick an issue that you want to tackle. If you want to make a change that's not already described by an issue, please make one first so we can keep track of who is working on what.
2. Make sure that you understand what work is required - if you are unsure about anything, don't hesitate to ask by commenting on the issue.
3. [Assign](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#assignee) the issue to yourself so others know not to start working on it.
4. [Create a branch from the issue](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-new-branch-from-an-issue). This will also create a [draft merge request](https://docs.gitlab.com/ee/user/project/merge_requests/drafts.html).
5. Commit changes to the code on your branch. We recommend seeking feedback early and often in your draft merge request.
6. Once you are happy with what you have done, [mark the merge request as ready](https://docs.gitlab.com/ee/user/project/merge_requests/drafts.html#mark-merge-requests-as-ready). You should also update the merge request description to explain your changes.

Your merge request will then be reviewed.

### Accepting merge requests

Before a merge request can be merged, by default it must:

- Be rebased onto master - this can often be done [through the GitLab UI](https://docs.gitlab.com/ee/topics/git/git_rebase.html#rebase-from-the-gitlab-ui), but you may need to do so [via the command line](https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase).
- Pass the pipelines without warnings.
- Be approved by at least one [maintainer](https://gitlab.com/m-c-moore/autoballot/-/project_members?sort=access_level_desc).

## Code quality checks

### Pre-commit

We use [pre-commit](https://pre-commit.com/) to run various code quality checks locally whenever changes are committed. These checks include:

- [flake8](https://flake8.pycqa.org/en/latest/) - PEP 8 style convention checker.
- [black](https://black.readthedocs.io/en/stable/) - automatic code formatter.
- [isort](https://pycqa.github.io/isort/) - automatic import sorter.
- [pydocstyle](http://www.pydocstyle.org/en/stable/) - PEP 257 docstring convention checker.
- Checks to prevent `noqa` or `type: ignore` comments that don't specify error codes.

To set up pre-commit, run

```sh
pip install pre-commit
pre-commit install
```

### CI pipelines

Whenever commits are pushed, a set of [pipelines](https://gitlab.com/m-c-moore/autoballot/-/pipelines) will run to ensure that all code is consistently formatted and documented, and that the unit, integration and benchmark tests pass. In addition to the pre-commit checks, the following are run on all branches (if changes are made to files in the relevant directories):

- [mypy](https://mypy.readthedocs.io/en/stable/) - static type checker.
- [pytest](https://docs.pytest.org/en/stable/) - runner for unit, integration and benchmark tests. Results and coverage reports are available as [job artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html).
- [pdoc3](https://pdoc3.github.io/pdoc/) - generates the [technical documentation](https://m-c-moore.gitlab.io/autoballot/src/) from the docstrings of the files in the `src/` directory.

On master, the outputs of the pdoc3 job are published as [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

Pipelines are a good way to quickly confirm that everything is working, although if they are failing it is much quicker to debug and fix them by running the checks locally. You can see what commands to run by looking at the job's `script:` tag in [.gitlab-ci.yml](.gitlab-ci.yml). For example, to run the `mypy` job locally, execute `python -m mypy` in a terminal.

## Privacy

This project works with potentially private and sensitive data, however **there is no need to share this data with anyone other than the individual running the ballot**. To aid with development, some representative mock data has been generated and can be found in `tests/mock_data/big/` - see the [user guide](user_guide.ipynb) for how to set this as the default input directory.

If you are working with sensitive data, it is obviously extremely important that none of it is uploaded to GitLab. To avoid accidentally committing sensitive files, keep them in the `data/` directory, which is ignored by git. Mistakes can happen, and if you do commit and push a sensitive file, it is vital that you [clean the git history](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/removing-sensitive-data-from-a-repository) - **just deleting the file and committing that as a change is not sufficient**. If you are unsure how to do this, please contact Matt or the JCR Internet Officer.

## License

By contributing to this project, you agree that your contributions will be licensed under its [license](LICENSE).
