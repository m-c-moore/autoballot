cmake==3.22.0
freezegun==1.1.0
lapsolver==1.1.0
mypy==0.931
numba==0.55.0
numpy==1.20.3
pandas==1.3.5
pdoc3==0.10.0
pre-commit==2.16.0
pytest==6.2.5
pytest-benchmark==3.4.1
pytest-cov==3.0.0
pytest-html==3.1.1
python-dotenv==0.19.2
tqdm==4.62.3
types-freezegun==1.1.6

# Optional: dtale, notebook, pdoc3
