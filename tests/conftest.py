import os

import pandas  # noqa: F401 # need to import before using freeze_time
from freezegun import freeze_time


def pytest_sessionstart() -> None:
    os.environ.setdefault("INPUT_DIR", "tests/mock_data/small")

    # mock current date as this affects renovation date mapping
    freezer = freeze_time("2021-03-08", ignore=["pandas"])
    freezer.start()
