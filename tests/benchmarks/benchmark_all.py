"""Benchmarks for the main workflow.

Use the `--benchmark-disable` flag to run each benchmark function once,
running the tests but not reporting any stats. Caches will be persisted
to increase speed.
"""

import os
import typing
from unittest import mock

import pandas as pd
import pytest

from src.ballot.evaluate import analyse_single_solution, generate_and_analyse_solutions
from src.ballot.match import StudentMatches, SyndicateMatches
from src.ballot.shuffle import BlockConfig, expand_configs, find_valid_configs
from src.read import RoomData, StudentData, read_room_data, read_student_data


class ResultStorage:
    valid_configs: list[BlockConfig]
    valid_seeds: list[int]
    expanded_valid_configs: list[BlockConfig]
    stats: pd.DataFrame


@pytest.fixture(scope="module", autouse=True)
def mock_environ() -> typing.Any:
    with mock.patch.dict(os.environ, dict(INPUT_DIR="tests/mock_data/big")) as _fixture:
        yield _fixture


@pytest.fixture(scope="module")
def room_data() -> RoomData:
    return read_room_data()


@pytest.fixture(scope="module")
def student_data() -> StudentData:
    return read_student_data()


@pytest.fixture(scope="module")
def student_matches() -> StudentMatches:
    return StudentMatches()


@pytest.fixture(scope="module")
def syndicate_matches(student_matches: StudentMatches) -> SyndicateMatches:
    return SyndicateMatches(student_matches)


def _clear_caches(student_matches: StudentMatches, syndicate_matches: SyndicateMatches) -> None:
    """Clear cached matches.

    Note that multiple rounds / iterations may still make use of caches.
    """
    student_matches.match_cache.clear()
    syndicate_matches.match_cache.clear()
    syndicate_matches.matches_cache.clear()


def benchmark_find_valid_configs(
    benchmark: typing.Any,
    room_data: RoomData,
    student_data: StudentData,
) -> None:
    num_seeds = 1000

    valid_configs, valid_seeds = benchmark.pedantic(
        find_valid_configs,
        kwargs=dict(
            room_data=room_data,
            student_data=student_data,
            seeds=list(range(num_seeds)),
            verbose=False,
        ),
        warmup_rounds=0,
        iterations=1,
        rounds=3,
    )

    assert len(valid_seeds) >= num_seeds / 500, "at least 0.2% success rate"
    ResultStorage.valid_configs = valid_configs
    ResultStorage.valid_seeds = valid_seeds


def benchmark_expand_configs(benchmark: typing.Any) -> None:
    expansion_factor = 10

    expanded_valid_configs = benchmark.pedantic(
        expand_configs,
        kwargs=dict(
            valid_configs=ResultStorage.valid_configs,
            factor=expansion_factor,
            verbose=False,
        ),
        warmup_rounds=0,
        iterations=1,
        rounds=10,
    )

    assert len(expanded_valid_configs) == expansion_factor * len(ResultStorage.valid_seeds)
    ResultStorage.expanded_valid_configs = expanded_valid_configs


@pytest.mark.parametrize("analysis_depth", [0, 1, 2, 3])
def benchmark_generate_and_analyse_solutions(
    pytestconfig: typing.Any,
    benchmark: typing.Any,
    room_data: RoomData,
    student_data: StudentData,
    student_matches: StudentMatches,
    syndicate_matches: SyndicateMatches,
    analysis_depth: int,
) -> None:
    if not pytestconfig.getoption("benchmark_disable"):
        _clear_caches(student_matches, syndicate_matches)

    stats = benchmark.pedantic(
        generate_and_analyse_solutions,
        kwargs=dict(
            room_data=room_data,
            student_data=student_data,
            valid_configs=ResultStorage.expanded_valid_configs,
            syndicate_matches=syndicate_matches,
            include_feature_counts=analysis_depth >= 1,
            include_price_counts=analysis_depth >= 1,
            include_cost_stats=analysis_depth >= 2,
            include_price_stats=analysis_depth >= 2,
            include_metrics=analysis_depth >= 3,
            get_all_e_metrics=analysis_depth >= 3,
            drop_invalid=True,
            verbose=False,
        ),
        warmup_rounds=0,
        iterations=1,
        rounds=3,
    )

    assert len(stats) >= len(ResultStorage.expanded_valid_configs) / 5, "at least 20% success rate"
    ResultStorage.stats = stats


def benchmark_analyse_single_solution(
    pytestconfig: typing.Any,
    benchmark: typing.Any,
    room_data: RoomData,
    student_data: StudentData,
    student_matches: StudentMatches,
    syndicate_matches: SyndicateMatches,
) -> None:
    best_index = ResultStorage.stats["tot_cost"].idxmin()
    best_row = ResultStorage.stats.loc[best_index]

    if not pytestconfig.getoption("benchmark_disable"):
        _clear_caches(student_matches, syndicate_matches)

    solution = benchmark.pedantic(
        analyse_single_solution,
        kwargs=dict(
            room_data=room_data,
            student_data=student_data,
            indices=best_row["indices"],
            student_matches=student_matches,
        ),
        warmup_rounds=0,
        iterations=1,
        rounds=10,
    )

    assert len(solution) == len(student_data.students)
