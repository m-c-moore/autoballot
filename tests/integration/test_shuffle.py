"""Integration tests for the `src.ballot.shuffle` module."""

import pytest

from src.ballot.shuffle import BlockConfig, expand_configs, find_valid_configs
from src.read import RoomData, StudentData, read_room_data, read_student_data

# =========== fixtures =========== #


@pytest.fixture
def room_data() -> RoomData:
    return read_room_data()


@pytest.fixture
def student_data() -> StudentData:
    return read_student_data()


@pytest.fixture
def seeds() -> list[int]:
    return list(range(100))


@pytest.fixture
def valid_configs_and_seeds(
    room_data: RoomData, student_data: StudentData, seeds: list[int]
) -> tuple[list[BlockConfig], list[int]]:
    return find_valid_configs(room_data, student_data, seeds, verbose=True)


@pytest.fixture
def valid_configs(valid_configs_and_seeds: tuple[list[BlockConfig], list[int]]) -> list[BlockConfig]:
    return valid_configs_and_seeds[0]


# =========== assertions =========== #


def _assert_config_equal(config_1: BlockConfig, config_2: BlockConfig) -> None:
    assert config_1.seed == config_2.seed
    assert config_1.blocks_by_size == config_2.blocks_by_size
    assert config_1.split_blocks == config_2.split_blocks


# =========== tests =========== #


def test_find_valid_configs(valid_configs: list[BlockConfig]) -> None:
    assert len(valid_configs) > 0


def test_expand_configs(valid_configs: list[BlockConfig]) -> None:
    factor = 4
    expanded = expand_configs(valid_configs, factor, verbose=True)
    assert len(expanded) == len(valid_configs) * factor


def test_regenerate_valid_configs(
    valid_configs_and_seeds: tuple[list[BlockConfig], list[int]],
    room_data: RoomData,
    student_data: StudentData,
    seeds: list[int],
) -> None:
    """Test that configurations can be recovered from their random seed(s) alone.

    This is very important!
    """

    configs, valid_seeds = valid_configs_and_seeds
    new_configs, _ = find_valid_configs(room_data, student_data, valid_seeds, verbose=True)

    for i in range(len(configs)):
        _assert_config_equal(configs[i], new_configs[i])
