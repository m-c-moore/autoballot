from src.ballot.match import StudentMatches, SyndicateMatches
from src.read import read_room_data, read_student_data


def test_correct_allocations() -> None:
    """Matches should be "along the diagonal", i.e. Aardvark -> A, Badger -> B etc.

    If the match cost calculation is changed, the rooms / students may need to be
    changed to make this test pass.
    """
    room_data = read_room_data()
    student_data = read_student_data()

    syndicate_matches = SyndicateMatches(StudentMatches())
    match = syndicate_matches.match(student_data.syndicates[0], room_data.blocks[0])
    assert len(match.matches) == student_data.syndicates[0].size

    for i, student_match in enumerate(match.matches):
        assert student_match.student == student_data.students[i]
        assert student_match.room == room_data.rooms[i]
