"""Integration tests for the `src.ballot.match` module.

Note that only the first 7 students and rooms (A-G) are included in this test.
"""

import copy
import dataclasses
import typing
from unittest import mock

import numpy as np
import pytest

from src.ballot.match import StudentMatches
from src.entities import BoolFeatures, Room, Student
from src.params import ASYM_PREFS, CONSTANTS, SYM_PREFS, VALID_ASYM_PREF_VALUES, VALID_SYM_PREF_VALUES
from src.read import read_room_data, read_student_data

MAX_ENTITIES = 7

# =========== fixtures =========== #


def _get_rooms() -> list[Room]:
    return read_room_data().rooms[:MAX_ENTITIES]


def _get_students() -> list[Student]:
    return read_student_data().students[:MAX_ENTITIES]


@pytest.fixture(params=_get_rooms())
def room(request: typing.Any) -> Room:
    return request.param


@pytest.fixture(params=_get_students())
def student(request: typing.Any) -> Student:
    return request.param


@pytest.fixture(params=BoolFeatures.get_field_names())
def bool_feature(request: typing.Any) -> str:
    return request.param


@pytest.fixture
def student_matches() -> typing.Generator[StudentMatches, None, None]:
    # prevent matches from being cached
    with mock.patch.object(StudentMatches, "__setitem__", lambda *args: None):
        yield StudentMatches()


# =========== helper functions =========== #


def _seeded_rng(student_: Student, room_: Room, feature_name: str = None) -> np.random.Generator:
    seed_str = student_.id + room_.id
    if feature_name is not None:
        seed_str += feature_name

    return np.random.default_rng([ord(c) for c in seed_str])


T = typing.TypeVar("T", Student, Room)


def _copy_entity(entity: T) -> T:
    return copy.deepcopy(entity)


def _update_pref_or_feat(
    student_or_room: typing.Union[Student, Room],
    field_name: str,
    field_value: typing.Union[float, bool],
) -> None:
    if isinstance(student_or_room, Student):
        student_or_room._prefs = dataclasses.replace(student_or_room.prefs, **{field_name: field_value})
    else:
        student_or_room._feats = dataclasses.replace(student_or_room.feats, **{field_name: field_value})


def _rand_preference(
    rng: np.random.Generator,
    student_: Student,
    pref_name: str,
    lower_or_higher: typing.Literal["lower", "higher"],
) -> float:
    current_pref_value = getattr(student_.prefs, pref_name)
    valid_pref_values = VALID_SYM_PREF_VALUES if pref_name in {"house", "long_lease"} else VALID_ASYM_PREF_VALUES

    if lower_or_higher == "lower":
        possible_new_pref_values = filter(lambda v: v < current_pref_value, valid_pref_values)
    elif lower_or_higher == "higher":
        possible_new_pref_values = filter(lambda v: v > current_pref_value, valid_pref_values)
    else:
        raise ValueError(lower_or_higher)

    return rng.choice(list(possible_new_pref_values))


def _get_new_cost(
    student_matches: StudentMatches,
    student_1: Student,
    room_1: Room,
    new_student_or_room: typing.Union[Student, Room],
    /,
    *,
    absolute: bool = False,
) -> typing.Tuple[float, float]:
    old_cost = student_matches.match(student_1, room_1).cost
    if isinstance(new_student_or_room, Student):
        new_cost = student_matches.match(new_student_or_room, room_1).cost
    else:
        new_cost = student_matches.match(student_1, new_student_or_room).cost

    if absolute:
        old_cost = abs(old_cost)
        new_cost = abs(new_cost)
    return old_cost, new_cost


# =========== assertions =========== #


def _assert_match_cost_equal(*args: typing.Any) -> None:
    old_cost, new_cost = _get_new_cost(*args, absolute=False)
    assert old_cost == new_cost


def _assert_match_cost_increased(*args: typing.Any, tolerance: float = 0.0, **kwargs: bool) -> None:
    old_cost, new_cost = _get_new_cost(*args, **kwargs)
    assert (old_cost == new_cost == np.inf) or old_cost < new_cost + tolerance


def _assert_match_cost_decreased(*args: typing.Any, tolerance: float = 0.0, **kwargs: bool) -> None:
    old_cost, new_cost = _get_new_cost(*args, **kwargs)
    assert (old_cost == new_cost == np.inf) or old_cost > new_cost - tolerance


# =========== tests =========== #


def test_price_penalties_arbitrarily_high(student: Student, room: Room, student_matches: StudentMatches) -> None:
    """Cost values should be very high where students overpay significantly."""
    if room.feats.price >= student.prefs.acc_price + 15:
        assert student_matches.match(student, room).cost > 1000


def test_missing_requirement_has_infinite_cost(student: Student, room: Room, student_matches: StudentMatches) -> None:
    """Cost values should be infinite where students are missing a requirement."""
    if student.reqs is not None and student.reqs.is_missing_requirements(room.feats):
        assert student_matches.match(student, room).cost == np.inf


def test_changing_preference_cost_correct(
    student: Student,
    room: Room,
    student_matches: StudentMatches,
    bool_feature: str,
) -> None:
    rng = _seeded_rng(student, room, bool_feature)

    pref_value = getattr(student.prefs, bool_feature)
    feat_value = getattr(room.feats, bool_feature)

    new_student = _copy_entity(student)
    new_room = _copy_entity(room)

    # There is an interesting edge case where changing a preference between 'definitely' and 'a lot'
    # can do the opposite of what one would expect to the match cost (e.g. if room has a feature and
    # the preference becomes weaker, the cost still decreases) due to the penalisation of lots of
    # strong preferences. To capture this behaviour, we allow the cost to move in the opposite
    # direction by a small amount.
    def get_tolerance(pref_value_: float, new_pref_value_: float) -> float:
        if {pref_value_, new_pref_value_} == {ASYM_PREFS.definitely.value, ASYM_PREFS.a_lot.value}:
            return 3.0
        return 0.0

    if pref_value > SYM_PREFS.indifferent.value:
        new_pref_value = _rand_preference(rng, student, bool_feature, "lower")
        _update_pref_or_feat(new_student, bool_feature, new_pref_value)
        tolerance = get_tolerance(pref_value, new_pref_value)

        # preference & feature
        if feat_value > 0.5:
            # decreasing met preference -> increased cost
            _assert_match_cost_increased(student_matches, student, room, new_student, tolerance=tolerance)

            # removing preferred feature -> increased cost
            _update_pref_or_feat(new_room, bool_feature, False)
            _assert_match_cost_increased(student_matches, student, room, new_room)

        # preference & !feature
        else:
            # decreasing unmet preference -> decreased cost
            _assert_match_cost_decreased(student_matches, student, room, new_student, tolerance=tolerance)

            # adding preferred feature -> decreased cost
            _update_pref_or_feat(new_room, bool_feature, True)
            _assert_match_cost_decreased(student_matches, student, room, new_room)

    # includes indifferent
    else:
        new_pref_value = _rand_preference(rng, student, bool_feature, "higher")
        _update_pref_or_feat(new_student, bool_feature, new_pref_value)
        tolerance = get_tolerance(pref_value, new_pref_value)

        # !preference & feature
        if feat_value > 0.5:
            # increasing met preference -> decreased cost
            _assert_match_cost_decreased(student_matches, student, room, new_student, tolerance=tolerance)

            # removing disliked feature -> decreased cost. Indifferent -> no change
            _update_pref_or_feat(new_room, bool_feature, False)
            if getattr(student.prefs, bool_feature) == SYM_PREFS.indifferent.value:
                _assert_match_cost_equal(student_matches, student, room, new_room)
            else:
                _assert_match_cost_decreased(student_matches, student, room, new_room)

        # !preference & !feature
        else:
            # increasing unmet preference -> increased cost
            _assert_match_cost_increased(student_matches, student, room, new_student, tolerance=tolerance)

            # adding disliked feature -> increased cost. Indifferent -> no change
            _update_pref_or_feat(new_room, bool_feature, True)
            if getattr(student.prefs, bool_feature) == SYM_PREFS.indifferent.value:
                _assert_match_cost_equal(student_matches, student, room, new_room)
            else:
                _assert_match_cost_increased(student_matches, student, room, new_room)


def test_changing_size_cost_correct(student: Student, room: Room, student_matches: StudentMatches) -> None:
    rng = _seeded_rng(student, room)
    feat_name = "size"

    new_student = _copy_entity(student)
    new_room = _copy_entity(room)

    # room too small
    if student.prefs.min_size > room.feats.size:
        pref_name = "min_size"

        # decrease min size -> decreased cost
        new_pref_value = rng.integers(CONSTANTS.min_room_size.value, student.prefs.min_size)
        _update_pref_or_feat(new_student, pref_name, new_pref_value)
        _assert_match_cost_decreased(student_matches, student, room, new_student)

        # increase room size -> decreased cost
        new_feat_value = rng.integers(room.feats.size + 1, student.prefs.max_size)
        _update_pref_or_feat(new_room, feat_name, new_feat_value)
        _assert_match_cost_decreased(student_matches, student, room, new_room)

    elif student.prefs.max_size < room.feats.size:
        pref_name = "max_size"

        # increase max size -> decreased cost
        new_pref_value = rng.integers(student.prefs.max_size + 1, CONSTANTS.max_room_size.value + 1)
        _update_pref_or_feat(new_student, pref_name, new_pref_value)
        _assert_match_cost_decreased(student_matches, student, room, new_student)

        # decrease room size -> decreased cost
        new_feat_value = rng.integers(student.prefs.min_size + 1, room.feats.size - 1)
        _update_pref_or_feat(new_room, feat_name, new_feat_value)
        _assert_match_cost_decreased(student_matches, student, room, new_room)

    else:
        # change room size within range -> no change
        new_feat_value = rng.integers(student.prefs.min_size, student.prefs.max_size)
        _update_pref_or_feat(new_room, feat_name, new_feat_value)
        _assert_match_cost_equal(student_matches, student, room, new_room)


def test_changing_rank_cost_correct(student: Student, room: Room, student_matches: StudentMatches) -> None:
    rng = _seeded_rng(student, room)
    new_student = _copy_entity(student)

    if student_matches.match(student, room).cost != 0:
        # higher rank -> larger cost magnitude
        if student.rank - 1 > 1:
            new_student._rank = rng.integers(1, student.rank)
            _assert_match_cost_increased(student_matches, student, room, new_student, absolute=True)

        # lower rank -> smaller cost magnitude
        if student.rank + 1 < CONSTANTS.max_rank.value:
            new_student._rank = rng.integers(student.rank + 1, CONSTANTS.max_rank.value + 1)
            _assert_match_cost_decreased(student_matches, student, room, new_student, absolute=True)

    else:
        # zero cost -> no change with rank
        new_student._rank = rng.integers(1, CONSTANTS.max_rank.value + 1)
        _assert_match_cost_equal(student_matches, student, room, new_student)
