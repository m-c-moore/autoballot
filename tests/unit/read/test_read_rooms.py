import typing

import pytest

from src.entities import Room
from src.read import read_room_data


@pytest.fixture(params=read_room_data().rooms)
def room(request: typing.Any) -> Room:
    return request.param


@pytest.mark.parametrize(
    "field_name,expected_column",
    [
        ("id", ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N"]),
        ("floor", [2, 1, 2, 3, 3, 1, 3, 3, 1, 2, 3, 2, 1, 3]),
        ("band", [1, 10, 7, 5, 2, 5, 12, 2, 10, 7, 5, 1, 5, 12]),
        ("block.id", ["Z_1"] * 7 + ["Y_1", "Y_1", "Y_1", "Y_2", "Y_2", "Y_2", "Y_3"]),
        ("building.id", ["Z"] * 7 + ["Y"] * 7),
    ],
)
def test_read_rooms_reads_correct_parameters(room: Room, field_name: str, expected_column: list[float]) -> None:
    if field_name == "block.id":
        value = room.block.id
    elif field_name == "building.id":
        value = room.building.id
    else:
        value = getattr(room, field_name)
    assert value == expected_column[room.index]


@pytest.mark.parametrize(
    "field_name,expected_column",
    [
        ("double", [True, False, False, True, True, True, False, True, False, False, True, True, True, False]),
        ("house", [True, False, False, True, False, False, True, False, False, False, True, True, False, True]),
        ("long_lease", [True, False, False, True, True, False, True, True, False, False, True, True, False, True]),
        ("size", [18, 9, 14.5, 13, 25, 13.8, 12, 25, 9, 14.5, 13, 18, 13.8, 12]),
        ("price", [196, 143, 163, 173, 190, 173, 132, 190, 143, 163, 173, 196, 173, 132]),
    ],
)
def test_read_rooms_reads_correct_preferences(room: Room, field_name: str, expected_column: list[float]) -> None:
    assert getattr(room.feats, field_name) == expected_column[room.index]
