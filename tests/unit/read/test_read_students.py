import typing

import pytest

from src.entities import Requirements, Student
from src.read import read_student_data


@pytest.fixture(params=read_student_data().students)
def student(request: typing.Any) -> Student:
    return request.param


@pytest.mark.parametrize(
    "field_name,expected_column",
    [
        (
            "id",
            [
                "Aardvark",
                "Badger",
                "Camel",
                "Dolphin",
                "Elephant",
                "Fox",
                "Gorilla",
                "Hamster",
                "Iguana",
                "Jellyfish",
                "Kangaroo",
                "Lemur",
                "Moose",
                "Narwhal",
            ],
        ),
        ("year", [2] * 14),
        ("rank", [1, 2, 3, 4, 5, 6, 6, 1, 2, 3, 4, 5, 6, 6]),
        ("syndicate.id", [1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 3, 3, 4, 5]),
    ],
)
def test_read_students_reads_correct_parameters(
    student: Student, field_name: str, expected_column: list[float]
) -> None:
    if field_name == "syndicate.id":
        value = int(student.syndicate.id)
    else:
        value = getattr(student, field_name)
    assert value == expected_column[student.index]


@pytest.mark.parametrize(
    "field_name,expected_column",
    [
        ("double", [0.6, -0.2, 0.4, 0.4, 0.6, 0.2, -0.2, 0.6, -0.2, 0.4, 0.4, 0.6, 0.2, -0.2]),
        ("house", [0.5, 0.5, -0.5, -0.2, -0.5, 0.5, 0.5, 0.5, 0.5, -0.5, -0.2, -0.5, 0.5, 0.5]),
        ("min_size", [18, 9, 13, 14, 18, 14, 9, 18, 9, 13, 14, 18, 14, 9]),
        ("price_perc_over", [15, 12, 15, 22, 10, 14, 14, 15, 12, 15, 22, 10, 14, 14]),
    ],
)
def test_read_students_reads_correct_preferences(
    student: Student, field_name: str, expected_column: list[float]
) -> None:
    assert getattr(student.prefs, field_name) == expected_column[student.index]


def test_read_students_reads_correct_requirements(student: Student) -> None:
    requirements_by_student: dict[str, dict[str, typing.Any]] = dict(
        Camel=dict(ensuite=True, min_size=12),
        Gorilla=dict(long_lease=True),
    )

    try:
        expected_requirements = requirements_by_student[student.id]
        for field_name in Requirements.get_field_names():
            requirement = getattr(student.reqs, field_name)
            try:
                assert requirement == expected_requirements[field_name]
            except KeyError:
                assert not requirement

    except KeyError:
        assert student.reqs is None
