import pandas as pd

from src.scripts.parse import parse_form


def test_user_ids_return_to_original_values() -> None:
    user_ids = pd.Series(data=[5, 50, 100, 150, 200])
    student_ids = parse_form._user_id_to_student_id(user_ids)
    converted_ids = parse_form._student_id_to_user_id(student_ids)

    assert (converted_ids == user_ids).all()

    form_data = pd.DataFrame(
        data=[
            [2, "a", "b"],
            [50, "c", "d"],
        ],
        columns=["user_id", "fieldA", "fieldB"],
    )

    form_data["student_id"] = parse_form._user_id_to_student_id(form_data["user_id"])
    form_data["converted_id"] = parse_form._student_id_to_user_id(form_data["student_id"])
    assert (form_data["converted_id"] == form_data["user_id"]).all()
