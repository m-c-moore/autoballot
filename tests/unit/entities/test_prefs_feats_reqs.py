import typing

import pytest

from src.entities import Features, Preferences, Requirements
from src.entities.feats_prefs_reqs import NEW_FEATURES
from src.exceptions import InvalidInputError
from src.params import CONSTANTS, SYM_PREFS

# =========== Features =========== #


@pytest.fixture(params=Features.get_field_names())
def features_field_name(request: typing.Any) -> str:
    return request.param


@pytest.fixture
def default_features_kwargs() -> dict[str, typing.Any]:
    bool_kwargs = {field_name: True for field_name in Features.get_bool_field_names()}
    num_kwargs = dict(size=CONSTANTS.max_room_size.value, price=CONSTANTS.max_room_price.value)
    return bool_kwargs | num_kwargs


def test_invalid_features_value_raises(
    features_field_name: str, default_features_kwargs: dict[str, typing.Any]
) -> None:
    invalid_value: typing.Any

    if features_field_name == "size":
        invalid_value = CONSTANTS.max_room_size.value + 1
    elif features_field_name == "price":
        invalid_value = CONSTANTS.max_room_price.value + 1
    elif features_field_name in NEW_FEATURES:
        invalid_value = 1.2
    else:
        invalid_value = 0.7

    kwargs = default_features_kwargs | {features_field_name: invalid_value}
    with pytest.raises(InvalidInputError):
        Features(**kwargs)


# =========== Preferences =========== #


@pytest.fixture(params=Preferences.get_field_names())
def preferences_field_name(request: typing.Any) -> str:
    return request.param


@pytest.fixture
def default_preferences_kwargs() -> dict[str, typing.Any]:
    bool_kwargs = {field_name: SYM_PREFS.indifferent.value for field_name in Preferences.get_bool_field_names()}
    num_kwargs = dict(
        min_size=CONSTANTS.min_room_size.value,
        max_size=CONSTANTS.max_room_size.value,
        est_price=CONSTANTS.min_room_price.value,
        price_perc_over=CONSTANTS.min_price_perc_over.value,
    )
    return bool_kwargs | num_kwargs


def test_invalid_preferences_value_raises(
    preferences_field_name: str,
    default_preferences_kwargs: dict[str, typing.Any],
) -> None:
    invalid_value: typing.Any

    if preferences_field_name == "min_size":
        invalid_value = CONSTANTS.min_room_size.value - 1
    elif preferences_field_name == "max_size":
        invalid_value = CONSTANTS.max_room_size.value + 1
    elif preferences_field_name == "est_price":
        invalid_value = CONSTANTS.min_room_price.value - 1
    elif preferences_field_name == "price_perc_over":
        invalid_value = CONSTANTS.min_price_perc_over.value - 1
    else:
        invalid_value = 0.3

    kwargs = default_preferences_kwargs | {preferences_field_name: invalid_value}
    with pytest.raises(InvalidInputError):
        Preferences(**kwargs)


def test_incompatible_preferences_values_raises(default_preferences_kwargs: dict[str, typing.Any]) -> None:
    kwargs = default_preferences_kwargs | dict(
        min_size=CONSTANTS.min_room_size.value + 5,
        max_size=CONSTANTS.max_room_size.value + 3,
    )

    with pytest.raises(InvalidInputError):
        Preferences(**kwargs)


# =========== Requirements =========== #


@pytest.fixture(params=Requirements.get_field_names())
def requirements_field_name(request: typing.Any) -> str:
    return request.param


@pytest.fixture
def default_requirements_kwargs() -> dict[str, typing.Any]:
    bool_kwargs = {field_name: False for field_name in Requirements.get_bool_field_names()}
    num_kwargs = dict(min_size=None)
    return bool_kwargs | num_kwargs


def test_invalid_requirements_value_raises(
    requirements_field_name: str,
    default_requirements_kwargs: dict[str, typing.Any],
) -> None:
    invalid_value: typing.Any

    if requirements_field_name == "min_size":
        invalid_value = CONSTANTS.max_room_size.value + 1
    else:
        invalid_value = 0.7

    kwargs = default_requirements_kwargs | {requirements_field_name: invalid_value}
    with pytest.raises(InvalidInputError):
        Requirements(**kwargs)


def test_incompatible_requirements_values_raises(default_requirements_kwargs: dict[str, typing.Any]) -> None:
    kwargs = default_requirements_kwargs | dict(ground=True, upper=True)

    with pytest.raises(InvalidInputError):
        Requirements(**kwargs)


@pytest.fixture(params=Requirements.get_field_names())
def bool_requirements_field_name(request: typing.Any) -> str:
    return request.param


def test_met_requirements_are_identified(
    default_requirements_kwargs: dict[str, typing.Any],
    default_features_kwargs: dict[str, typing.Any],
) -> None:

    requirements_kwargs: dict[str, typing.Any] = {}
    for field_name in default_requirements_kwargs.keys():
        if field_name == "min_size":
            requirements_kwargs[field_name] = CONSTANTS.min_room_size.value
        elif field_name == "ground":
            requirements_kwargs[field_name] = False  # can't have ground and upper
        else:
            requirements_kwargs[field_name] = True

    requirements = Requirements(**requirements_kwargs)
    features = Features(**default_features_kwargs)
    assert not requirements.is_missing_requirements(features)


def test_unmet_requirements_are_identified(
    requirements_field_name: str,
    default_requirements_kwargs: dict[str, typing.Any],
    default_features_kwargs: dict[str, typing.Any],
) -> None:
    features_kwargs = default_features_kwargs.copy()
    requirements_kwargs = default_requirements_kwargs.copy()

    if requirements_field_name == "min_size":
        features_kwargs["size"] = CONSTANTS.min_room_size.value + 3
        requirements_kwargs["min_size"] = CONSTANTS.min_room_size.value + 5
    elif requirements_field_name == "ground":
        features_kwargs["upper"] = True
        requirements_kwargs["ground"] = True
    else:
        features_kwargs[requirements_field_name] = False
        requirements_kwargs[requirements_field_name] = True

    requirements = Requirements(**requirements_kwargs)
    features = Features(**features_kwargs)
    assert requirements.is_missing_requirements(features)
