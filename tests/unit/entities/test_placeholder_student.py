from src.ballot.match import StudentMatches
from src.entities.utils import add_placeholder_student
from src.read import read_room_data, read_student_data


def test_consistent_match_cost() -> None:
    """Cost values should be the same in every room."""
    room_data = read_room_data()
    student_data = read_student_data()
    placeholder_student = add_placeholder_student(student_data)

    student_matches = StudentMatches()
    costs = {student_matches.match(placeholder_student, room).cost for room in room_data.rooms}
    assert len(costs) == 1
