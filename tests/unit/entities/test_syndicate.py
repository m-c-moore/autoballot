import pytest

from src.entities import Preferences, Student, Syndicate
from src.params import CONSTANTS, SYM_PREFS


def new_student(index: int) -> Student:
    preferences = Preferences(
        **{field_name: SYM_PREFS.indifferent.value for field_name in Preferences.get_bool_field_names()},
        min_size=CONSTANTS.min_room_size.value,
        max_size=CONSTANTS.max_room_size.value,
        est_price=CONSTANTS.max_room_price.value,
        price_perc_over=CONSTANTS.max_price_perc_over.value,
    )
    return Student(f"id_{index}", index=index, rank=1, year=1, prefs=preferences, reqs=None)


# =========== syndicate.add_child() =========== #


@pytest.mark.parametrize("size", [1, 3, 8, 100])
def test_syndicate_size_correct(size: int) -> None:
    syndicate = Syndicate("a")

    for i in range(size):
        syndicate.add_child(new_student(i + 1))

    syndicate.finalise()
    assert syndicate.size == size


def test_add_to_finalised_syndicate_raises() -> None:
    syndicate = Syndicate("a")
    syndicate.add_child(new_student(1))
    syndicate.finalise()

    with pytest.raises(AssertionError):
        syndicate.add_child(new_student(2))


def test_add_to_syndicate_twice_raises() -> None:
    student = new_student(1)
    syndicate = Syndicate("a")

    syndicate.add_child(student)
    with pytest.raises(AssertionError):
        syndicate.add_child(student)


def test_add_to_two_syndicates_raises() -> None:
    student = new_student(1)
    syndicate_1 = Syndicate("a")
    syndicate_2 = Syndicate("b")

    syndicate_1.add_child(student)
    with pytest.raises(AssertionError):
        syndicate_2.add_child(student)


# =========== syndicate getters =========== #


def test_non_finalised_syndicate_getters_raise() -> None:
    syndicate = Syndicate("a")

    with pytest.raises(AssertionError):
        _ = syndicate.size
