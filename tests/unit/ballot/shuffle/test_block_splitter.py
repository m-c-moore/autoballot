import typing

import numpy as np
import pytest

from src.ballot.shuffle import RandomnessLevel
from src.ballot.shuffle.block_splitter import BlockSplitter
from src.exceptions import NoMoreSplitsError
from src.params import CONSTANTS


@pytest.fixture(params=tuple(range(1, CONSTANTS.block_size_limit.value + 1)))
def block_size(request: typing.Any) -> int:
    return request.param


@pytest.fixture(params=list(RandomnessLevel))
def randomness(request: typing.Any) -> RandomnessLevel:
    return request.param


@pytest.fixture
def block_splitter(randomness: RandomnessLevel) -> BlockSplitter:
    return BlockSplitter(randomness)


@pytest.fixture
def expected_number_of_splits(block_size: int) -> int:
    # see https://en.wikipedia.org/wiki/Partition_%28number_theory%29
    return [1, 1, 2, 3, 5, 7, 11, 15, 22, 30, 42, 56, 77, 101, 135, 176, 231][block_size] - 1


@pytest.fixture
def generated_splits(block_splitter: BlockSplitter, block_size: int) -> list[tuple[int, ...]]:
    block_splitter.reset()
    rng = np.random.default_rng(block_size)

    splits = []
    previous_split_invalid = False

    while True:
        try:
            split, _ = block_splitter.get_split(
                block_size,
                previous_split_invalid=previous_split_invalid,
                rand_unif=rng.random(),
            )
        except NoMoreSplitsError:
            break

        splits.append(split)
        previous_split_invalid = True

    return splits


def test_split_counts_correct(generated_splits: list[tuple[int, ...]], expected_number_of_splits: int) -> None:
    assert len(generated_splits) == expected_number_of_splits


def test_split_sums_correct(generated_splits: list[tuple[int, ...]], block_size: int) -> None:
    assert all(sum(split) == block_size for split in generated_splits)


def test_get_split_doesnt_return_duplicates(generated_splits: list[tuple[int, ...]]) -> None:
    assert len(set(generated_splits)) == len(generated_splits)
