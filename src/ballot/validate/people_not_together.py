"""File containing the `check_people_not_together()` function."""

import os

import pandas as pd


def check_people_not_together(solution_df: pd.DataFrame) -> None:
    """Print warning messages if people are living together who can't."""

    print("\nChecking certain people not together:")
    dir_path = os.environ.get("INPUT_DIR", os.path.join("data", "parsed_inputs"))

    df = pd.read_csv(f"{dir_path}/not_together.csv")
    buildings = solution_df.set_index("student")["building"]
    max_len = max(df["student_id_1"].str.len().max(), df["student_id_2"].str.len().max())

    valid = True

    for _, row in df.iterrows():
        id_1 = row["student_id_1"]
        id_2 = row["student_id_2"]

        try:
            building_1 = buildings[id_1]
        except KeyError:
            print(f"  {id_1} - not found")
            continue

        try:
            building_2 = buildings[id_2]
        except KeyError:
            print(f"  {id_2} - not found")
            continue

        if building_1 == building_2:
            valid = False
            if row["extent"] == "building":
                print(f"  {id_1:>{max_len}} X {id_2:<{max_len}}   SAME BUILDING")
            else:
                print(f"  {id_1:>{max_len}} ? {id_2:<{max_len}}   maybe SAME BLOCK (please check)")
        else:
            print(f"  {id_1:>{max_len}} - {id_2:<{max_len}}   OK")

    if valid:
        print("\n====== ALL OK ======")
    else:
        print("\nxxxxx NOT OK! xxxxx")
