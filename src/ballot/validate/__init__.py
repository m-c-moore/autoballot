"""Module containing functions to check the validity of the solution."""

from .people_not_together import check_people_not_together

__all__ = ["check_people_not_together"]
