"""File containing the `calc_z_scores()` function."""
import numpy as np


def calc_z_scores(array: np.ndarray) -> np.ndarray:
    """Return the z scores of each element in the `array`."""
    mean = np.mean(array)
    std_dev = np.std(array)

    if std_dev == 0.0:
        raise ZeroDivisionError

    return (array - mean) / std_dev
