"""File containing the `get_solution_counts()` function."""

import typing
from collections import defaultdict

from src.ballot.match import SyndicateMatch
from src.entities import BoolFeatures


def get_solution_counts(
    solved_matches: list[SyndicateMatch],
    stats_row: defaultdict[str, typing.Any],
    include_feature_counts: bool,
    include_price_counts: bool,
    cost_list: typing.Optional[list[float]],
    price_perc_over_list: typing.Optional[list[float]],
) -> None:
    """Populate the `stats_row` with counts of students who got each feature.

    Parameters
    ----------
    solved_matches
        List of solved `SyndicateMatch` instances. Each will have its indices updated.
    stats_row
        Dictionary defining a row of the stats dataframe.
    include_feature_counts
        Set to `True` to include counts of students who got each feature.
    include_price_counts
        Set to `True` to include counts of students in each price preference bracket.
    cost_list
        If supplied, will have each student's match cost appended.
    price_perc_over_list
        If supplied, will have each student's overpay percentage appended.
    """
    for syndicate_match in solved_matches:
        syndicate_match.update_indices(stats_row["indices"])
        for match in syndicate_match.matches:
            student = match.student
            room = match.room
            price_perc = (room.feats.price - student.prefs.est_price) / student.prefs.est_price * 100

            if include_feature_counts:
                for field_name in list(BoolFeatures.get_field_names()) + ["staircase"]:
                    if student.has_preference(field_name) and room.has_feature(field_name):
                        stats_row[field_name] += 1

                if student.prefs.min_size <= room.feats.size <= student.prefs.max_size:
                    stats_row["size"] += 1

            if include_price_counts:
                if price_perc <= 0:
                    stats_row["no_overpay"] += 1

                if 0 < price_perc <= student.prefs.price_perc_over:
                    stats_row["acceptable_overpay"] += 1

                if price_perc > student.prefs.price_perc_over:
                    stats_row["overpay_0_plus"] += 1

                if price_perc > student.prefs.price_perc_over + 3:  # "outlier" is 3% over acceptable
                    stats_row["overpay_3_plus"] += 1

            if cost_list is not None:
                cost_list.append(match.cost)

            if price_perc_over_list is not None:
                price_perc_over_list.append(price_perc - student.prefs.price_perc_over)
