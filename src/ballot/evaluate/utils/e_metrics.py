"""File containing the `calc_efficiency_metrics()` function."""

from __future__ import annotations

from collections import defaultdict

import numpy as np
import pandas as pd
from tqdm import tqdm

from src.entities import BoolFeatures
from src.params import VECTORS
from src.read import RoomData, StudentData


def calc_efficiency_metrics(
    room_data: RoomData,
    student_data: StudentData,
    stats_df: pd.DataFrame,
    get_all: bool = False,
) -> None:
    """Add efficiency metric columns to the stats dataframe in place.

    This measures the efficiency of the algorithm at allocating certain features.
    Denoting the number of students wanting a certain feature as `x`, number of rooms
    with that feature `y` and number of students who received that feature as `z`, the
    efficiency metric is given by `E = z / min(x, y) * 100`.

    I.e., it measures how efficiently the algorithm allocated features to those who
    wanted them given the potentially limited supply of that features. If the number
    who received that feature is equal to either the number of students who wanted it
    OR the number of rooms with it, the algorithm is considered 100% efficient.

    Parameters
    ----------
    room_data:
        Input room data.
    student_data:
        Input student data.
    stats_df
        Statistics dataframe.
    get_all
        If `True`, will add columns for every feature. Otherwise, just the overall
        efficiency metric is calculated (this is a weighted average of all the
        individual efficiency metrics).
    """

    total_features: defaultdict[str, int] = defaultdict(int)
    total_preferences: defaultdict[str, int] = defaultdict(int)

    field_names = list(BoolFeatures.get_field_names()) + ["staircase"]

    for field_name in field_names:
        for room in room_data.rooms:
            total_features[field_name] += int(room.has_feature(field_name))

        for student in student_data.students:
            total_preferences[field_name] += int(student.has_preference(field_name))

    e_metrics_by_field: defaultdict[str, np.ndarray] = defaultdict(lambda: np.zeros(len(stats_df)))
    weighted_e_metric = np.zeros(len(stats_df))

    for i in tqdm(range(len(stats_df))):
        if not stats_df.iloc[i]["valid"]:
            continue

        for field_name in field_names:
            min_total = min(total_features[field_name], total_preferences[field_name])
            if min_total == 0:
                continue

            # e metric by field
            e_metric = stats_df.iloc[i][field_name] / min_total * 100
            e_metrics_by_field["e_" + field_name][i] = e_metric

            # weighted average of field e metrics
            if field_name == "staircase":
                weight = VECTORS.house.value
            else:
                weight = getattr(VECTORS, field_name).value
            weighted_e_metric[i] += e_metric * weight

    stats_df["e_metric"] = weighted_e_metric / (sum(w.value for w in VECTORS) + VECTORS.house.value)

    if get_all:
        for column, values in e_metrics_by_field.items():
            stats_df[column] = values
