"""File containing the `analyse_dataframe_in_dtale()` function."""

import os
import time
import typing
import warnings

import pandas as pd


def analyse_dataframe_in_dtale(df: pd.DataFrame, save: bool = True) -> typing.Any:
    """Load the given dataframe in dtale, and optionally save it to a timestamped csv."""
    try:
        import dtale
    except ModuleNotFoundError:
        warnings.warn("dtale is not installed. Run `pip install dtale`.")
        return

    if save:
        time_str = time.strftime("%Y-%m-%d_%H-%M-%S")
        dir_path = os.environ.get("OUTPUT_DIR", os.path.join("data", "saved"))
        file_name = os.path.join(dir_path, f"{time_str}_({len(df)}).csv")
        df.drop(["config"], axis=1, errors="ignore").to_csv(file_name)

    d = dtale.show(
        df.drop(["indices", "config"], axis=1, errors="ignore"), allow_cell_edits=False, ignore_duplicate=True
    )
    d.open_browser()
    return d
