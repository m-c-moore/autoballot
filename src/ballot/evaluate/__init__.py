"""Module containing functions to optimise and evaluate block configurations."""

from .analyse_in_dtale import analyse_dataframe_in_dtale
from .analyse_solution import analyse_single_solution
from .generate_solutions import generate_and_analyse_solutions

__all__ = ["analyse_dataframe_in_dtale", "analyse_single_solution", "generate_and_analyse_solutions"]
