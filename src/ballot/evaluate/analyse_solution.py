"""File containing the `analyse_single_solution()` function."""

import typing

import numpy as np
import pandas as pd

from src.ballot.match import StudentMatches
from src.entities import Features
from src.entities.feats_prefs_reqs import SYMMETRIC_FEATURES
from src.params import ASYM_PREFS, SYM_PREFS
from src.read import RoomData, StudentData


def analyse_single_solution(
    room_data: RoomData,
    student_data: StudentData,
    indices: np.ndarray,
    student_matches: StudentMatches,
) -> pd.DataFrame:
    """Return a dataframe of stats analysing a single solution on a student-by-student basis.

    Parameters
    ----------
    room_data:
        Input room data.
    student_data:
        Input student data.
    indices
        Array of indices, where the index is the room index and the value is the
        index of the student assigned to that room.
    student_matches
        `StudentMatches` instance.
    """
    stats: dict[int, dict[str, typing.Union[int, float, bool, str]]] = {}

    print("Empty rooms:\n\t" + ", ".join(sorted(room_data.rooms[i[0]].id for i in np.argwhere(indices == -1))))

    for i, room in enumerate(room_data.rooms):
        index = int(indices[room.index])
        if index == -1:
            continue

        student = student_data.students[index]
        match = student_matches.match(student, room)

        did_want_did_get = []
        did_want_didnt_get = []
        didnt_want_did_get = []
        didnt_want_didnt_get = []

        for field_name in Features.get_field_names():
            if field_name in SYMMETRIC_FEATURES:  # handled below
                continue

            if field_name == "price":  # handled elsewhere
                continue

            if field_name == "size":
                pref = ASYM_PREFS.a_lot.value
                got = student.prefs.min_size <= room.feats.size

            else:
                pref = getattr(student.prefs, field_name)
                got = room.has_feature(field_name)

            # capitalise strong preferences
            if pref in {ASYM_PREFS.definitely.value, ASYM_PREFS.not_at_all.value}:
                field_name = field_name.upper()

            if pref >= ASYM_PREFS.a_lot.value:
                if got:
                    did_want_did_get.append(field_name)
                else:
                    did_want_didnt_get.append(field_name)
            elif pref <= SYM_PREFS.probably_not.value:
                if got:
                    didnt_want_did_get.append(field_name)
                else:
                    didnt_want_didnt_get.append(field_name)

        lease_house = []
        for positive, negative, field_name in [
            ("long", "short", "long_lease"),
            ("house", "staircase", "house"),
        ]:
            pref = getattr(student.prefs, field_name)
            got = getattr(room.feats, field_name)

            if pref == SYM_PREFS.positive.value:
                lease_house.append(("GOT_" if got else "MISS_") + positive)
            elif pref == SYM_PREFS.probably.value:
                lease_house.append(("got_" if got else "miss_") + positive)
            elif pref == SYM_PREFS.probably_not.value:
                lease_house.append(("miss_" if got else "got_") + negative)
            elif pref == SYM_PREFS.negative.value:
                lease_house.append(("MISS_" if got else "GOT_") + negative)
            else:
                lease_house.append("indifferent")

        perc_over_acc = (
            100 * (room.feats.price - student.prefs.est_price) / student.prefs.est_price - student.prefs.price_perc_over
        )

        stats[i] = dict(
            student=student.id,
            room=room.id,
            cost=match.cost,
            feat_cost=match.feature_cost,
            size_cost=match.size_cost,
            price_cost=match.price_cost,
            building=room.building.id,
            block=room.block.id,
            rank=student.rank,
            syn_id=student.syndicate.id,
            syn_size=student.syndicate.size,
            lease=lease_house[0],
            house=lease_house[1],
            did_want_did_get=",".join(did_want_did_get),
            did_want_didnt_get=",".join(did_want_didnt_get),
            didnt_want_did_get=",".join(didnt_want_did_get),
            didnt_want_didnt_get=",".join(didnt_want_didnt_get),
            size=room.feats.size,
            est_price=student.prefs.est_price,
            acc_price=student.prefs.acc_price,
            room_price=room.feats.price,
            perc_over_acc=perc_over_acc,
            reqs=str(student.reqs),
        )

    return pd.DataFrame.from_dict(stats, orient="index")
