"""File containing the `generate_and_analyse_solutions()` function."""

from __future__ import annotations

import time
import typing
from collections import defaultdict

import numpy as np
import pandas as pd
from tqdm import tqdm

from src.ballot.match import SyndicateMatch, SyndicateMatches
from src.ballot.shuffle import BlockConfig
from src.entities import Syndicate
from src.read import RoomData, StudentData

from .utils.e_metrics import calc_efficiency_metrics
from .utils.solution_counts import get_solution_counts
from .utils.z_scores import calc_z_scores


def generate_and_analyse_solutions(
    room_data: RoomData,
    student_data: StudentData,
    valid_configs: list[BlockConfig],
    syndicate_matches: SyndicateMatches,
    *,
    include_feature_counts: bool = False,
    include_price_counts: bool = False,
    include_cost_stats: bool = False,
    include_price_stats: bool = False,
    include_metrics: bool = False,
    get_all_e_metrics: bool = False,
    drop_invalid: bool = False,
    verbose: bool = True,
) -> pd.DataFrame:
    """Return a dataframe of statistics summarising the solution for each configuration.

    Parameters
    ----------
    room_data:
        Input room data.
    student_data:
        Input student data.
    valid_configs
         List of valid block configurations (i.e. syndicate sizes match block sizes).
    syndicate_matches
        `SyndicateMatches` instance.
    include_feature_counts
        Set to `True` to include counts of students who got each feature.
    include_price_counts
        Set to `True` to include counts of students in each price preference bracket.
    include_cost_stats
        Set to `True` to include stats about student-room match costs.
    include_price_stats
         Set to `True` to include stats about overpay percentages.
    include_metrics
        Set to `True` to include various solution metrics.
    get_all_e_metrics
        Set to `True` to include columns for individual feature efficiency metrics.
    drop_invalid
        Set to `True` to exclude configurations with infinite cost.
    verbose
        If `True`, prints messages as it runs.
    """
    start = time.time()

    if verbose:
        print(f"\nOptimising and evaluating {len(valid_configs)} configurations:")

    # dictionary that will become the statistics dataframe
    stats: defaultdict[int, defaultdict[str, typing.Any]] = defaultdict(lambda: defaultdict(int))

    # get syndicates by size
    syndicates_by_size: defaultdict[int, list[Syndicate]] = defaultdict(lambda: [])
    for syndicate in student_data.syndicates:
        syndicates_by_size[syndicate.size].append(syndicate)

    num_rooms = len(room_data.rooms)
    num_valid = 0
    pbar = tqdm(valid_configs)
    for i, config in enumerate(pbar):
        # basic stats
        stats[i]["seed"] = config.seed
        stats[i]["exp_seed"] = config.exp_seed
        stats[i]["splits"] = len(config.split_blocks)
        stats[i]["valid"] = True
        stats[i]["reason"] = None
        stats[i]["config"] = config
        stats[i]["indices"] = 0 - np.ones(num_rooms, dtype=np.int16)

        # cost list, use to calculate cost stats
        cost_list: typing.Optional[list[float]] = [] if include_cost_stats else None
        price_perc_over_list: typing.Optional[list[float]] = [] if include_price_stats else None

        # iterate through syndicate / block sizes and optimally solve the assignment problem for each
        for size in syndicates_by_size.keys():
            cost, res = syndicate_matches.solve_syndicate_assignment(
                syndicates_by_size[size], config.blocks_by_size[size]
            )

            # cancel if failed to assign syndicate
            if cost == np.inf:
                stats[i]["valid"] = False
                stats[i]["reason"] = tuple(res)
                break

            stats[i]["tot_cost"] += cost
            solved_matches: list[SyndicateMatch] = res  # type: ignore[assignment]

            get_solution_counts(
                solved_matches,
                stats[i],
                include_feature_counts,
                include_price_counts,
                cost_list,
                price_perc_over_list,
            )

        # skip if invalid
        if stats[i]["valid"]:
            num_valid += 1
            pbar.set_postfix_str(f"{num_valid / (i + 1):.1%} valid")
        else:
            continue

        # calculate cost stats
        if cost_list is not None:
            cost_array = np.array(cost_list)
            stats[i]["cost_std"] = np.std(cost_array)
            stats[i]["cost_max"] = np.amax(cost_array)
            stats[i]["cost_outliers"] = sum(calc_z_scores(cost_array) > 2)

        # calculate price stats
        if price_perc_over_list is not None:
            stats[i]["price_perc_std"] = np.std(price_perc_over_list)
            stats[i]["price_perc_max"] = np.amax(price_perc_over_list)

    stats_df = pd.DataFrame.from_dict(stats, orient="index")
    reasons_count = stats_df.groupby("reason").count()["config"]

    if drop_invalid:
        stats_df = stats_df[stats_df["valid"] == True]  # noqa: E712

    if include_metrics:
        if not include_feature_counts:
            print("\tWARN: Cannot calculate E-metric without feature counts")
        else:
            calc_efficiency_metrics(room_data, student_data, stats_df, get_all_e_metrics)

    if verbose:
        print(f"\tFinished in {time.time() - start:.3f} s.")
        print(f"\t{num_valid} / {len(valid_configs)} were valid.")
        print("\tReasons for failure:")
        print("\t\t" + reasons_count.to_string(header=False).replace("\n", "\n\t\t"))

    return stats_df
