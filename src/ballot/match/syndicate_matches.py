"""File containing the `SyndicateMatch` and `SyndicateMatches` classes."""

from __future__ import annotations

import numpy as np

from src.entities import Block, Syndicate
from src.exceptions import MatchError

from .base_matches import Match, Matches, TAssignmentResult
from .student_matches import StudentMatch, StudentMatches


class SyndicateMatch(Match[Syndicate, Block]):
    """Class representing a syndicate-block match. Used to calculate the match cost."""

    def __init__(self, match_id: str, syndicate: Syndicate, block: Block, student_matches: StudentMatches):
        """Initialise a `SyndicateMatch` instance. Calculates the cost immediately."""
        self.matches: list[StudentMatch] = []
        self._student_matches = student_matches

        super().__init__(match_id, syndicate, block)

    @property
    def syndicate(self) -> Syndicate:
        """The syndicate being matched to a block."""
        return self._source

    @property
    def block(self) -> Block:
        """The block being matched to a syndicate."""
        return self._dest

    def _calculate(self) -> None:
        """Assign students to rooms and calculate the cost of the match."""
        self.cost, result = self._student_matches.solve_student_assignment(self.syndicate.students, self.block.rooms)

        if self.cost == np.inf:
            self.valid = False
            self.reason = frozenset(result)  # type: ignore[arg-type]
        else:
            self.matches = result  # type: ignore[assignment]

    def update_indices(self, indices: np.ndarray) -> None:
        """Update the student-room indices array with the syndicate's allocations."""
        if len(self.matches) == 0:
            raise MatchError("Cannot update indices without any matches")

        for match in self.matches:
            if indices[match.room.index] != -1:
                raise MatchError("Cannot set index that has already been set")
            indices[match.room.index] = match.student.index


class SyndicateMatches(Matches[SyndicateMatch]):
    """Class to manage a collection of `SyndicateMatch` instances."""

    def __init__(self, student_matches: StudentMatches):
        """Initialise a `SyndicateMatches` instance."""
        super().__init__(SyndicateMatch, student_matches)
        self.matches_cache: dict[int, TAssignmentResult] = {}

    def solve_syndicate_assignment(self, syndicates: list[Syndicate], blocks: list[Block]) -> TAssignmentResult:
        """Solve the syndicate-block assignment problem.

        The first return value is the cost of the match. The second is a list of syndicate
        matches if the algorithm was successful, otherwise it is a set of reasons for why
        it failed.
        """
        cache_key = hash(frozenset(s.id for s in syndicates).union(frozenset(b.id for b in blocks)))

        try:
            return self.matches_cache[cache_key]
        except KeyError:
            cost_and_result = self._solve_assignment(syndicates, blocks)
            self.matches_cache[cache_key] = cost_and_result
            return cost_and_result
