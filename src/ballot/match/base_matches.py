"""File containing the `Match` and `Matches` abstract base classes."""

from __future__ import annotations

import typing
from abc import ABC, abstractmethod

import numpy as np

from src.entities import Block, Room, Student, Syndicate
from src.exceptions import ImpossibleAssignmentError, MatchError

from .linear_sum_assignment import solve_linear_sum_assignment

TSource = typing.TypeVar("TSource", Student, Syndicate)
TDest = typing.TypeVar("TDest", Room, Block)
TMatch = typing.TypeVar("TMatch", bound="Match")

# TODO use namedtuple
TReason = typing.Union[str, typing.FrozenSet[str]]
TAssignmentResult = typing.Tuple[float, typing.Union[typing.List[TMatch], typing.Set[TReason]]]


class Match(ABC, typing.Generic[TSource, TDest]):
    """Abstract base class from which `StudentMatch` and `SyndicateMatch` are extended."""

    def __init__(self, match_id: str, source: TSource, dest: TDest):
        """Initialise a `Match` instance. Calculates the cost immediately."""
        self.id = match_id
        self._source: TSource = source
        self._dest: TDest = dest

        self.cost = 0.0  # cost of the match
        self.reason: TReason = "n/a"  # reason for invalid match (if applicable)
        self._calculate()

    def __repr__(self) -> str:
        return f"{self.id} = {self.cost:.2f}"

    @abstractmethod
    def _calculate(self) -> None:
        """Calculate the cost of the match."""
        pass


class Matches(ABC, typing.Generic[TMatch]):
    """Abstract base class from which `StudentMatches` and `SyndicateMatches` are extended.

    It is used to store a map of `Match` subclass instances to avoid recalculating as
    this tends to be computationally expensive.
    """

    def __init__(self, match_cls: typing.Type[TMatch], *match_cls_args: typing.Any):
        """Initialise a `Matches` instance. This should be done once.

        Parameters
        ----------
        match_cls
            `Match` subclass, instances of which will be returned by `Matches.match()`.
        match_cls_args
            Arguments to be passed to `match_cls.__init__()` in addition to `match_id`,
            `source` and `dest`.
        """
        self.match_cache: dict[str, TMatch] = {}
        self._match_cls = match_cls
        self._match_cls_args = match_cls_args

    def __repr__(self) -> str:
        return "\n".join(str(match) for match in self.match_cache.values())

    def __getitem__(self, item: str) -> TMatch:
        return self.match_cache[item]

    def __setitem__(self, key: str, value: TMatch) -> None:
        if key in self.match_cache:
            raise MatchError(f"Match with key '{key}' already in cache")

        self.match_cache[key] = value

    def match(self, source: TSource, dest: TDest) -> TMatch:
        """Return the `Match` subclass instance for the given `source` and `dest`.

        If this match has already been made, return the original match. Otherwise,
        initialise a new one and store it.
        """
        match_id = f"{source.id} -> {dest.id}"

        try:
            return self[match_id]
        except KeyError:
            match = self._match_cls(match_id, source, dest, *self._match_cls_args)
            self[match_id] = match
            return match

    def _solve_assignment(self, source_list: list[TSource], dest_list: list[TDest]) -> TAssignmentResult:
        # build cost matrix
        cost_matrix = np.zeros((len(source_list), len(dest_list)))
        reasons: set[TReason] = set()

        for i, source in enumerate(source_list):
            all_inf = True

            for j, dest in enumerate(dest_list):
                match = self.match(source, dest)
                cost_matrix[i, j] = match.cost

                if match.cost == np.inf:
                    reasons.add(match.reason)
                all_inf = False

            if all_inf:
                return np.inf, reasons

        # calculate optimal assignment
        try:
            rows, cols = solve_linear_sum_assignment(cost_matrix)
        except ImpossibleAssignmentError:
            return np.inf, reasons

        # build results
        cost = 0.0
        matches = []

        for i in range(len(rows)):
            source = source_list[rows[i]]
            dest = dest_list[cols[i]]

            match = self.match(source, dest)
            matches.append(match)
            cost += match.cost

        return cost, matches
