"""Module containing functions and classes to calculate allocation costs.

This module handles the calculations for both student-room and syndicate-block
allocations. In each case, once a match has been made and the associated cost
calculated, it will be stored and reused to avoid repeating the same calculations.
"""

from .student_matches import StudentMatch, StudentMatches
from .syndicate_matches import SyndicateMatch, SyndicateMatches

__all__ = ["StudentMatch", "StudentMatches", "SyndicateMatch", "SyndicateMatches"]
