"""File containing the `StudentMatch` and `StudentMatches` classes."""

import numpy as np

from src.entities import Room, Student
from src.params import CONSTANTS, WEIGHTS

from .base_matches import Match, Matches, TAssignmentResult
from .costs import calc_est_price_cost, calc_feature_match_cost, calc_size_cost


class StudentMatch(Match[Student, Room]):
    """Class representing a student-room match. Used to calculate the match cost."""

    def __init__(self, match_id: str, student: Student, room: Room):
        """Initialise a `StudentMatch` instance. Calculates the cost immediately."""
        self.feature_cost = 0.0
        self.price_cost = 0.0
        self.size_cost = 0.0

        super().__init__(match_id, student, room)

    @property
    def student(self) -> Student:
        """The student that is being matched to a room."""
        return self._source

    @property
    def room(self) -> Room:
        """The room that is being matched to a student."""
        return self._dest

    def _calculate(self) -> None:
        """Calculate the cost of the match."""
        if self.student.reqs is not None and self.student.reqs.is_missing_requirements(self.room.feats):
            self.cost = np.inf
            self.reason = self.student.id
            return

        # calculate costs
        self.feature_cost = calc_feature_match_cost(self.student, self.room)
        self.size_cost = calc_size_cost(self.student, self.room)
        self.price_cost = calc_est_price_cost(self.student, self.room)

        # reduce price cost if have requirement for ensuite
        if self.student.reqs is not None:
            self.price_cost /= 1 + self.student.reqs.ensuite

        # scale with ballot rank, mapped to number between 1 and (1 + WEIGHTS.rank)
        n = CONSTANTS.max_rank.value
        self.cost = (self.feature_cost + self.size_cost + self.price_cost) * (
            1 + WEIGHTS.rank.value * (n - self.student.rank) / n
        )


class StudentMatches(Matches[StudentMatch]):
    """Class to manage a collection of `StudentMatch` instances."""

    def __init__(self) -> None:
        """Initialise a `StudentMatches` instance."""
        super().__init__(StudentMatch)

    def solve_student_assignment(self, students: list[Student], rooms: list[Room]) -> TAssignmentResult:
        """Solve the student-room assignment problem within a syndicate / block.

        The first return value is the cost of the match. The second is a list of student
        matches if the algorithm was successful, otherwise it is a set of reasons for why
        it failed.
        """
        return self._solve_assignment(students, rooms)
