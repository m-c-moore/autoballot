"""File containing the `solve_linear_sum_assignment()` function."""

from __future__ import annotations

import numpy as np
from lapsolver import solve_dense

from src.exceptions import ImpossibleAssignmentError, MatchError


def solve_linear_sum_assignment(cost_matrix: np.ndarray) -> tuple[np.ndarray, np.ndarray]:
    """Return the row and column indices of the optimal linear sum assignments.

    Return (`None`, []) if infeasible.

    Parameters
    ----------
    cost_matrix
        Cost matrix to solve. Must be wide.
    """
    if cost_matrix.shape[0] > cost_matrix.shape[1]:
        raise MatchError(f"Matrix must be wide, but has shape '{cost_matrix.shape}'")

    rows, columns = solve_dense(cost_matrix)

    if len(rows) != len(cost_matrix):
        raise ImpossibleAssignmentError()

    return rows, columns
