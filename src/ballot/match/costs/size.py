"""File containing the `calc_size_cost()` function."""

from src.entities import Room, Student
from src.params import WEIGHTS


def calc_size_cost(student: Student, room: Room) -> float:
    """Return the cost of assigning a student to a room based on their size preference.

    Notes
    -----
    Narrow ranges are weighted less strongly than wide ranges.
    """
    size_range_factor = ((student.prefs.max_size - student.prefs.min_size + 1) / 31) ** WEIGHTS.size_range.value

    if room.feats.size < student.prefs.min_size:
        return (student.prefs.min_size - room.feats.size) * WEIGHTS.smaller.value * size_range_factor

    if room.feats.size > student.prefs.max_size:
        return (room.feats.size - student.prefs.max_size) * WEIGHTS.larger.value * size_range_factor

    return 0.0
