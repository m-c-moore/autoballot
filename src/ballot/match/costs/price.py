"""File containing the `calc_est_price_cost()` function."""

import numpy as np

from src.entities import Room, Student
from src.params import SYM_PREFS, WEIGHTS


def calc_est_price_cost(student: Student, room: Room) -> float:
    """Return the cost of assigning a student to a room based on their price preference.

    This is essentially an exponential penalty function.

    Notes
    -----
    For students who specifically requested short lease rooms, the total room price
    over the year will be considered (i.e. the weekly room price will be considered to
    be 36/30 = 1.2 times greater). For students who said they "probably" want a short
    lease room, the scaling factor is 33/30 = 1.1.
    """
    scaled_price: float = room.feats.price

    if room.feats.long_lease:
        if student.prefs.long_lease == SYM_PREFS.negative.value:
            scaled_price *= 1.2
        elif student.prefs.long_lease == SYM_PREFS.probably_not.value:
            scaled_price *= 1.1

    # calculate cost
    perc_diff = (scaled_price - student.prefs.est_price) / student.prefs.est_price * 100.0
    return WEIGHTS.overpay_offset.value * np.exp(
        WEIGHTS.overpay_mult.value * (perc_diff - student.prefs.price_perc_over)
    )
