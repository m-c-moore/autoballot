"""File containing the `calc_feature_match_cost()` function."""

from src.entities import Room, Student
from src.params import ASYM_PREFS, WEIGHTS


def calc_feature_match_cost(student: Student, room: Room) -> float:
    """Return the cost of assigning a student to a room based on their preferences.

    It considers the alignment of the preferences vector, but also penalises students
    missing multiple features that they wanted. To do this, the fraction of missing
    features is considered, which effectively means that students with few strong
    preferences are more likely to have them met.
    """

    # alignment of preferences. Feature weights are taken into account in `room.feats.vector`
    cost = -1 * (student.prefs.vector @ room.feats.vector)

    # penalise missing preferences (considering fraction)
    def_prefs = student.prefs.vector == ASYM_PREFS.definitely.value
    a_lot_prefs = student.prefs.vector == ASYM_PREFS.a_lot.value
    missing_features = room.feats.vector < 0  # TODO

    frac_def = sum(def_prefs & missing_features) / max(1.0, sum(def_prefs))  # need max(1, ...) if sum(...) == 0
    frac_a_lot = sum(a_lot_prefs & missing_features) / max(1.0, sum(a_lot_prefs))  # need max(1, ...) if sum(...) == 0

    eff_frac = 1 + frac_def + frac_a_lot * WEIGHTS.missed_a_lot.value
    cost += WEIGHTS.missed_mult.value * eff_frac**WEIGHTS.missed_exp.value

    return cost
