"""File containing the `expand_config()` and `expand_configs()` functions."""

from __future__ import annotations

import random
import time
import typing
from collections import defaultdict

from tqdm import tqdm

from src.entities import Block, SBlock, SCBlock
from src.exceptions import BlockConfigError

from .block_config import BlockConfig


def expand_config(seed: int, valid_config: BlockConfig) -> BlockConfig:
    """Return a new valid configuration generated using the given one.

    This is achieved by randomly permuting the rooms in split blocks.
    """
    if valid_config.exp_seed is not None:
        raise BlockConfigError("Cannot expand a configuration that has already been expanded")

    random.seed(seed)

    new_blocks: dict[int, list[Block]] = defaultdict(lambda: [])
    new_split_blocks: dict[str, list[typing.Union[SBlock, SCBlock]]] = {}

    split_block_ids = set(block.id for blocks in valid_config.split_blocks.values() for block in blocks)

    # add unchanged blocks immediately
    # TODO loop through once only
    for size, blocks in valid_config.blocks_by_size.items():
        for block in blocks:
            if block.id not in split_block_ids:
                new_blocks[size].append(block)

    for split_blocks in valid_config.split_blocks.values():
        rooms = [room for block in split_blocks for room in block.rooms]
        random.shuffle(rooms)

        index = 0

        for block in split_blocks:
            if block.is_combined:
                new_blocks[block.size].append(SCBlock(rooms[index : block.size + index], block.building))
            else:
                new_blocks[block.size].append(SBlock(rooms[index : block.size + index], block.building))
            index += block.size

    return BlockConfig(valid_config.seed, seed, new_blocks, new_split_blocks)


def expand_configs(valid_configs: list[BlockConfig], factor: int, verbose: bool = True) -> list[BlockConfig]:
    """Return a list of new valid configurations generated using the given one.

    This is achieved by randomly permuting the rooms in split blocks in the
    `expand_config()` function `factor` times.
    """
    start = time.time()

    n = len(valid_configs)
    if verbose:
        print(f"\nShuffling rooms to generate {factor * n} more configurations:")

    new_configs: list[BlockConfig] = []

    for config in tqdm(valid_configs):
        for seed in range(factor):
            new_configs.append(expand_config(seed, config))

    if verbose:
        print(f"\tFinished in {time.time() - start:.3f} s.")

    return new_configs
