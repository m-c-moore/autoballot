"""File containing utility functions for the `BlockSplitter` class."""

import enum

import numpy as np
from numba import jit


class RandomnessLevel(enum.Enum):
    """Enum defining different levels of randomness."""

    low = enum.auto()
    medium = enum.auto()
    high = enum.auto()


def _find_combinations_recursive(
    res: list[tuple[int, ...]], arr: np.ndarray, index: int, num: int, reduced: int
) -> None:
    if reduced < 0:
        return

    if reduced == 0:
        res.append(tuple(arr[:index]))
        return

    prev = 1 if (index == 0) else arr[index - 1]

    for j in range(prev, num + 1):
        arr[index] = j
        _find_combinations_recursive(res, arr, index + 1, num, reduced - j)


def find_combinations(target: int) -> list[tuple[int, ...]]:
    """Return all combinations of positive numbers that add to the `target`.

    The combinations will be sorted descending by length (excluding the single length
    1 solution), and the values sorted ascending.

    Examples
    --------
    >>> find_combinations(5)
    [(1, 1, 1, 1, 1), (1, 1, 1, 2), (1, 1, 3), (1, 2, 2), (1, 4), (2, 3)]
    """
    results: list[tuple[int, ...]] = []
    arr = np.zeros(target, dtype=int)

    _find_combinations_recursive(results, arr, 0, target, target)
    return results[:-1]  # exclude last result (count = 1)


@jit(nopython=True)
def quick_choice(arr: np.ndarray, unnormalised_probs: np.ndarray, probs_sum: float, rand_unif: float) -> int:
    """Return a randomly selected element from `arr` with the given probabilities.

    This is optimised for a single draw so is faster than `np.random.choice`.
    """
    probs = unnormalised_probs / probs_sum
    assert np.around(np.sum(probs), 8) == 1.0

    cumulative_prob = 0.0

    for i, prob in enumerate(probs):
        cumulative_prob += prob
        if cumulative_prob > rand_unif:
            return arr[i]

    return arr[-1]
