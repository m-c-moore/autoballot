"""File containing the `BlockSplitter` class."""

from __future__ import annotations

import dataclasses
import typing

import numpy as np

from src.exceptions import NoMoreSplitsError
from src.params import CONSTANTS

from .split_utils import RandomnessLevel, find_combinations, quick_choice


@dataclasses.dataclass
class _PossibleSplits:
    size: int
    probability_exponent: float

    def __post_init__(self) -> None:
        self._splits = find_combinations(self.size)
        self._num_splits = len(self._splits)
        self._split_indices = np.arange(self._num_splits)

        self._split_size_diffs = np.zeros((self._num_splits, CONSTANTS.block_size_limit.value + 1), dtype=np.int16)
        for i, split in enumerate(self._splits):
            for split_size in split:
                self._split_size_diffs[i, split_size] += 1

        self._previous_split_index: typing.Optional[int] = None
        self.reset_probabilities()

    def reset_probabilities(self) -> None:
        self._split_probabilities = (1 + np.arange(self._num_splits)) ** self.probability_exponent
        self._probabilities_sum = sum(self._split_probabilities)

    def get_new_split(self, previous_split_invalid: bool, rand_unif: float) -> tuple[tuple[int, ...], np.ndarray]:
        # previously proposed one did not work -> don't propose it again
        if previous_split_invalid:
            self._probabilities_sum -= self._split_probabilities[self._previous_split_index]
            self._split_probabilities[self._previous_split_index] = 0

        # no more possible splits
        if round(self._probabilities_sum, 8) == 0.0:
            self._previous_split_index = None
            raise NoMoreSplitsError()

        split_index = quick_choice(self._split_indices, self._split_probabilities, self._probabilities_sum, rand_unif)
        self._previous_split_index = split_index
        return self._splits[split_index], self._split_size_diffs[split_index]


class BlockSplitter:
    """Class to generate ways to split a block.

    Should be instantiated once and reset with the `reset()` method.
    """

    def __init__(self, randomness: RandomnessLevel):
        """Initialise a `BlockSplitter` instance.

        Parameters
        ---------
        randomness
            How randomly the order of split proposals should be output. Can be set
            to "high", "medium" or "low". The higher the randomness, the less
            likely the proposals will be in the "useful" order, which tends to
            increase the variability of solutions but decrease the success rate in
            producing valid ones.

            In some cases, the original order may not actually be the most useful - it
            depends on the relative distributions of the block / syndicate sizes.
        """
        probability_exponent = {
            RandomnessLevel.low: 2,
            RandomnessLevel.medium: 1,
            RandomnessLevel.high: 0.5,
        }[randomness]

        self._splits_by_size: dict[int, _PossibleSplits] = {
            size: _PossibleSplits(size, probability_exponent) for size in range(1, CONSTANTS.block_size_limit.value + 1)
        }

    def get_split(self, block_size: int, **kwargs: typing.Any) -> tuple[tuple[int, ...], np.ndarray]:
        """Return a tuple of sub-block sizes and an array representing their sizes.

        The array of sizes can be used to easily see how the remaining syndicate sizes
        would be modified.

        Parameters
        ----------
        block_size
            Size of the block to split.

        **kwargs
            Passed to `_PossibleSplits.get_split`.

        Raises
        ------
        NoMoreSplitsError
            If there are no more splits available.
        """
        return self._splits_by_size[block_size].get_new_split(**kwargs)

    def reset(self) -> None:
        """Reset the probabilities. Call this whenever a new `BlockConfig` is generated."""
        for split in self._splits_by_size.values():
            split.reset_probabilities()
