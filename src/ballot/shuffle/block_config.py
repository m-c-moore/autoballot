"""File containing the `BlockConfig` and `BlockConfigGenerator` classes."""

from __future__ import annotations

import typing
from collections import defaultdict
from dataclasses import dataclass, field

import numpy as np

from src.entities import Block, SBlock, SCBlock
from src.exceptions import NoMoreSplitsError
from src.read import RoomData, StudentData

from .block_splitter import BlockSplitter
from .split_utils import RandomnessLevel

_MAX_GAP = 2


@dataclass(init=True, repr=True, eq=False, order=False, frozen=True)
class BlockConfig:
    """Class representing a single valid block configuration."""

    seed: int
    exp_seed: typing.Optional[int]
    blocks_by_size: dict[int, list[Block]] = field(repr=False)
    split_blocks: dict[str, list[typing.Union[SBlock, SCBlock]]] = field(repr=False)


class BlockConfigGenerator:
    """Class used to generate a block configuration using a random seed."""

    def __init__(
        self,
        room_data: RoomData,
        student_data: StudentData,
        seed: int,
        block_splitter: BlockSplitter,
        randomness: typing.Optional[RandomnessLevel],
        sort_desc: bool = False,
    ):
        """Initialise a block configuration generator.

        This shuffles a shallow copy of the blocks list using the given random seed.
        This means that the same random configuration will be created if the same
        parameters are used

        Parameters
        ----------
        room_data:
            Input room data.
        student_data:
            Input student data.
        seed
            Random seed for `np.random.default_rng()`.
        randomness
            How randomly the order of "use-combine-split" should be varied. Can be set
            to "high", "medium", "low" or None. The higher the randomness, the less
            likely the actions will be in their original order, which tends to increase
            the variability of solutions but decrease the success rate in producing
            valid ones.
        sort_desc
            If True, sorts blocks by size descending after shuffling. This greatly
            increases the percentage of valid configurations, but does decrease the
            randomness.
        """
        self._blocks = room_data.blocks.copy()  # copy as will be shuffled
        self._syn_size_left = student_data.syndicate_size_counts.copy()
        self._gaps_remaining = len(room_data.rooms) - len(student_data.students)
        self._max_syn_size = student_data.max_syndicate_size
        self._sort_desc = sort_desc
        self._block_splitter = block_splitter

        # initialise config variables
        self._used_block_ids: set[str] = set()
        self.blocks_by_size: dict[int, list[Block]] = defaultdict(lambda: [])

        # dictionary of block ids to its split blocks
        self.split_blocks: defaultdict[str, list[typing.Union[SBlock, SCBlock]]] = defaultdict(lambda: [])

        # random seed
        self.seed = seed
        self._rng = np.random.default_rng(self.seed)

        # generate random function orders. It is much more efficient to generate a lot in one go and reuse them
        self._funcs = np.array([self._use_if_useful, self._combine_if_useful, self._split_if_useful])
        self._func_order_index = 0

        if randomness is None:
            self._func_orders = np.array([[0, 1, 2]])
        else:
            # relative weights of function selection. Do not need to be normalised
            func_weights = {
                RandomnessLevel.low: [10, 5, 1],
                RandomnessLevel.medium: [4, 2, 1],
                RandomnessLevel.high: [1, 1, 1],
            }[randomness]

            # choice-without-replacement algorithm based on https://doi.org/10.1016/j.ipl.2005.11.003
            rand_unif = self._rng.random((20, 3))  # 20 is plenty, fine if they get reused
            self._func_orders = np.argsort(-np.log(rand_unif) / np.array(func_weights), axis=1)

    def _use_block(self, block: Block, eff_size: int) -> bool:
        gap_size = max(0, eff_size - self._max_syn_size)  # probably zero
        if gap_size > _MAX_GAP or gap_size > self._gaps_remaining:
            return False
        eff_size -= gap_size

        # use the block if it is a size we need
        if self._syn_size_left[eff_size] > 0:
            self._syn_size_left[eff_size] -= 1
            self.blocks_by_size[eff_size].append(block)

            self._gaps_remaining -= gap_size
            return True

        return False

    def _use_if_useful(self, index: int) -> bool:
        block = self._blocks[index]
        return self._use_block(block, block.size)

    def _combine_if_useful(self, index: int) -> bool:
        block = self._blocks[index]
        size = block.size
        if sum(self._syn_size_left[size + 1 :]) < 1:
            return False

        # look at the blocks in the same building that haven't been used
        # note this assumes that block.id is already in _used_block_ids
        remaining_blocks = [b for b in block.building.blocks if b.id not in self._used_block_ids]

        # attempt to combine with one other block.
        for r_block in remaining_blocks:
            # skip pre-allocated blocks
            if r_block.size == 0:
                continue

            # use if combined size is one that we need
            combined_block = block.combine(r_block)
            comb_size = combined_block.size
            if self._use_block(combined_block, comb_size):
                self._used_block_ids.add(r_block.id)
                return True

            # allow combined block be split with a syndicate of 1. We don't want
            # to perform any more complicated combine-and-splits as the students will
            # be "randomly" scattered throughout the effective block. Can't have gaps.
            if (
                comb_size - 2 <= self._max_syn_size
                and self._syn_size_left[comb_size - 1] > 0
                and self._syn_size_left[1] > 0
            ):
                new_combined, single = block.combine_and_split(r_block, block.rooms[0])
                self._used_block_ids.add(r_block.id)

                self.blocks_by_size[new_combined.size].append(new_combined)
                self.blocks_by_size[1].append(single)

                self.split_blocks[block.id].append(new_combined)
                self.split_blocks[block.id].append(single)

                self._syn_size_left[new_combined.size] -= 1
                self._syn_size_left[1] -= 1
                return True

            # TODO may need to combine >2 blocks if have lots of large syndicates

        return False

    def _split_if_useful(self, index: int) -> bool:
        block = self._blocks[index]
        previous_split_invalid = False
        size = block.size
        if sum(self._syn_size_left[:size]) < 2:
            return False

        while True:
            try:
                split, diff = self._block_splitter.get_split(
                    block.size,
                    previous_split_invalid=previous_split_invalid,
                    rand_unif=self._rng.random(),
                )
            except NoMoreSplitsError:
                return False

            # check useful split
            if all(self._syn_size_left - diff >= 0):
                self._syn_size_left -= diff

                # we don't shuffle the rooms, as will do so when we 'expand' it later
                index = 0
                rooms_lists = []
                for split_size in split:
                    rooms_lists.append(block.rooms[index : split_size + index])
                    index += split_size

                # TODO loop through once
                for split_block in block.split(rooms_lists):
                    self.blocks_by_size[split_block.size].append(split_block)
                    self.split_blocks[block.id].append(split_block)

                return True

            previous_split_invalid = True

    def _attempt_use_combine_split(self, index: int) -> bool:
        self._func_order_index += 1
        for func in self._funcs[self._func_orders[self._func_order_index % self._func_orders.shape[0]]]:
            if func(index):
                return True
        return False

    def _done(self) -> bool:
        return np.all(self._syn_size_left == 0)  # type: ignore[return-value]

    def is_valid(self) -> bool:
        """Return `True` if a valid configuration has been generated.

        This function iterates over the shuffled list of blocks, and will extract,
        combine or split each block as necessary.
        """
        self._rng = np.random.default_rng(self.seed)
        self._block_splitter.reset()

        # shuffle the list of blocks in place
        self._rng.shuffle(self._blocks)

        if self._sort_desc:
            self._blocks.sort(key=lambda b: b.size, reverse=True)

        # iterate through the shuffled blocks
        for i, block in enumerate(self._blocks):
            # skip blocks already used (as combined)
            if block.id in self._used_block_ids:
                continue

            self._used_block_ids.add(block.id)
            size = block.size

            # skip blocks of size 0 (i.e. fully pre-allocated)
            # note we do this after adding too used, to keep indices correct
            if size == 0:
                continue

            # attempt to use block, combine block, or split block in biased random order
            if self._attempt_use_combine_split(i):
                continue

            # block was not used, so remove from set
            self._used_block_ids.remove(block.id)
            self._gaps_remaining -= size

            if self._gaps_remaining < 0:
                return False

            if self._done():
                return True

        return self._done()

    def build(self) -> BlockConfig:
        """Return a `BlockConfig` instance using this generator's attributes."""
        return BlockConfig(self.seed, None, self.blocks_by_size, self.split_blocks)
