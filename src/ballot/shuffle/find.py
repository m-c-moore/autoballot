"""File containing the `find_valid_configs()` function."""

from __future__ import annotations

import time

import numpy as np
from tqdm import tqdm

from src.exceptions import ImpossibleBallotError
from src.read import RoomData, StudentData

from .block_config import BlockConfig, BlockConfigGenerator
from .block_splitter import BlockSplitter
from .split_utils import RandomnessLevel


def find_valid_configs(
    room_data: RoomData,
    student_data: StudentData,
    seeds: list[int],
    *,
    splitter_randomness: RandomnessLevel = RandomnessLevel.low,
    generator_randomness: RandomnessLevel = RandomnessLevel.low,
    verbose: bool = True,
) -> tuple[list[BlockConfig], list[int]]:
    """Return a list of valid configs and the seeds used to generate them.

    This is achieved by looping through the given seeds (should just be a range
    for the first round), and generating a new `BlockConfig` instance for each.
    """

    start = time.time()

    num_students = len(student_data.students)
    num_rooms = len(room_data.rooms)

    if num_students > num_rooms:
        raise ImpossibleBallotError(f"More students than rooms ({num_students} vs {num_rooms})")

    if verbose:
        with np.printoptions(formatter={"int": lambda n: f"{n:>3}"}):
            print(f"\nAttempting to fit {num_students} students into {num_rooms} rooms:")
            print(f"\tSyndicate sizes: {student_data.syndicate_size_counts}")
            print(f"\tBlock sizes:     {room_data.block_size_counts}")

    valid_configs = []
    valid_seeds = []
    block_splitter = BlockSplitter(splitter_randomness)

    pbar = tqdm(seeds)
    for i, seed in enumerate(pbar):
        block_config_gen = BlockConfigGenerator(
            room_data,
            student_data,
            seed,
            block_splitter,
            randomness=generator_randomness,
        )

        if block_config_gen.is_valid():
            valid_configs.append(block_config_gen.build())
            valid_seeds.append(seed)
            pbar.set_postfix_str(f"{len(valid_seeds)/(i+1):.1%} valid")

    if verbose:
        print(f"\tFinished in {time.time() - start:.4g} s.")
        perc = len(valid_configs) / len(seeds) * 100
        print(f"\t{len(valid_configs)} / {len(seeds)} ({perc:.1f}%) were valid.")

    return valid_configs, valid_seeds
