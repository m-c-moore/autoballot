"""Algorithm parameters and constants definitions.

Notes
-----
The values listed below are the final values that we used to run the program with.
Each parameter was carefully tuned by Matt and the JCR Committee in an attempt to
produce the fairest solution.

LOOKING AT THESE VALUES WITHOUT UNDERSTANDING HOW THEY ARE BEING USED WILL BE
MISLEADING. For example, `WEIGHTS.overpay_mult` is much smaller than
`VECTORS.ensuite`. This could imply that we are prioritising people wanting
ensuites over those wanting affordable rooms, but this is far from the truth (have a
look at `ballot/match/cost/price.py` to see why).
"""

from enum import Enum


class VECTORS(Enum):
    """Factors representing the relative importance of each feature to students.

    Increasing the factor of a room feature means that a student will be considered
    happier with the feature or sadder without it. For example, setting the
    value corresponding to the presence of a double bed to a higher number than that
    for an ensuite means that students will be assumed to care more about double beds.
    """

    double = 2.8
    ensuite = 2.6
    upper = 0.4
    not_faces_road = 1.2
    faces_court = 0.4
    new_kitchen = 0.6
    new_bathroom = 0.6
    new_room = 1.0
    house = 1.0
    long_lease = 2.0


class WEIGHTS(Enum):
    """Parameters used to set the weights of the variables in the match functions.

    Notes
    -----
    The relative sizes of these parameters do not necessarily give any indication of
    how important we felt each variable was. In order to understand that, it is
    necessary to look at how the parameters were used.
    """

    rank = 2.0  # x
    smaller = 2  # x
    larger = 0.8  # x
    size_range = 0.3  # ^

    overpay_offset = 5.0  # x
    overpay_mult = 0.8  # ^

    missed_exp = 3.0  # ^
    missed_mult = 1.0  # x
    missed_a_lot = 0.4  # x


class CONSTANTS(Enum):
    """Useful constants used throughout the program.

    It is likely that some of them will need to change each year.
    """

    max_rank = 6
    block_size_limit = 10  # also the syndicate size limit

    min_room_price = 132.0
    max_room_price = 214.0

    min_price_perc_over = 8
    max_price_perc_over = 100

    min_room_size = 9
    max_room_size = 40


class SYM_PREFS(Enum):
    """Mapping of symmetric preferences form responses to their values.

    Here, a 'symmetric' preference refers to a one where a negative form response
    corresponds to a positive preference for a different feature. For example, a
    negative preference for a long lease room corresponds to a positive preference
    for a short lease room.
    """

    negative = -0.5
    probably_not = -0.2
    indifferent = 0.0  # should equal SYM_PREFS.indifferent
    probably = 0.2
    positive = 0.5


class ASYM_PREFS(Enum):
    """Mapping of asymmetric preferences form responses to their values.

    Here, an 'asymmetric' preference refers to a one where a negative form response
    just represents a negative preference for that feature.
    """

    not_at_all = -0.4
    not_really = -0.2
    indifferent = SYM_PREFS.indifferent.value
    a_little = 0.2
    a_lot = 0.4
    definitely = 0.6


VALID_SYM_PREF_VALUES = {e.value for e in SYM_PREFS}
"""Set of valid values for symmetric preferences (see `SYM_PREFS`)."""

VALID_ASYM_PREF_VALUES = {e.value for e in ASYM_PREFS}
"""Set of valid values for asymmetric preferences (see `ASYM_PREFS`)."""
