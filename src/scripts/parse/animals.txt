aardvark
aardwolf
abyssinian
affenpinscher
akbash
akita
albatross
alligator
alpaca
anaconda
angelfish
angora
ant
anteater
antelope
antlion
ape
aphid
argentino
armadillo
asp
audemer
avocet
axolotl
aye
baboon
badger
balinese
bandicoot
barb
barnacle
barracuda
basilisk
bass
bat
beagle
bear
beaver
bedbug
bee
bee-eater
beetle
bernard
binturong
bird
birman
bison
bloodhound
blue
boa
boar
bobcat
bobolink
bombay
bongo
bonobo
booby
bordeaux
bovid
bracke
budgerigar
buffalo
bug
bulldog
bullfrog
burmese
butterfly
buzzard
caiman
camel
canid
canidae
capuchin
capybara
caracal
cardinal
caribou
carp
cassowary
cat
caterpillar
catfish
catshark
cattle
centipede
chameleon
chamois
cheetah
chicken
chihuahua
chimpanzee
chin
chinchilla
chinook
chipmunk
chow
cicada
cichlid
civet
clam
coati
cobra
cockroach
cod
collie
condor
coon
coonhound
coral
corgi
cougar
cow
coyote
crab
crane
crawdad
crayfish
cricket
crocodile
crow
cuckoo
cuscus
cuttlefish
dachsbracke
dachshund
dalmatian
dane
deer
devil
dhole
dingo
dinosaur
discus
dodo
dog
dogfish
dollar
dolphin
donkey
dormouse
dove
dragon
dragonfly
drever
duck
dugong
dunker
eagle
earwig
echidna
eel
egret
elephant
eleuth
elk
emperor
emu
ermine
falcon
felidae
ferret
finch
firefly
fish
flamingo
flea
flounder
fly
forest
fossa
fousek
fowl
fox
foxhound
frigatebird
frise
frog
gamefowl
gar
gayal
gazelle
gecko
gerbil
gharial
gibbon
giraffe
goat
goldfish
goose
gopher
gorilla
grasshopper
greyhound
grouse
guan
guanaco
gull
guppy
haddock
halibut
hamster
hare
harrier
havanese
hawk
hedgehog
heron
herring
himalayan
hippopotamus
hookworm
hornet
horse
hound
hoverfly
human
hummingbird
husky
hyena
hyrax
ibis
iguana
impala
indri
insect
jackal
jaguar
javanese
jay
jellyfish
kakapo
kangaroo
kingfisher
kite
kiwi
koala
koi
krill
kudu
labradoodle
ladybird
ladybug
lamprey
landfowl
lark
leech
lemming
lemur
leopard
leopon
liger
limpet
lion
lionfish
list
lizard
llama
lobster
locust
loon
louse
lynx
macaque
macaw
mackerel
magpie
malamute
maltese
mammal
mammoth
manatee
mandrill
markhor
marlin
marmoset
marmot
marten
mastiff
mastodon
mau
mayfly
meerkat
millipede
mink
minnow
mist
mite
mole
mollusk
molly
mongoose
mongrel
monkey
monster
moorhen
moose
mosquito
moth
mouse
mule
muskox
narwhal
neanderthal
newfoundland
newt
nightingale
numbat
ocelot
octopus
okapi
olm
opossum
orang-utan
orca
oriole
ostrich
otter
owl
ox
oyster
pademelon
panda
panther
paradise
parakeet
parrot
peacock
peafowl
peccary
pekingese
pelican
penguin
perch
persian
pheasant
pig
pigeon
pika
pike
pinniped
pinscher
piranha
platypus
pointer
pony
poodle
porcupine
porpoise
possum
prawn
primate
puffin
pug
puma
python
quail
quelea
quetzal
quokka
quoll
rabbit
raccoon
ragdoll
rat
rattlesnake
raven
ray
reindeer
reptile
retriever
rhinoceros
robin
rodent
rook
rooster
rottweiler
russel
sailfish
salamander
salmon
saola
sawfish
scallop
schnauzer
scorpion
seahorse
seal
serval
setter
shark
sheep
sheepdog
shepherd
shrew
shrimp
siamese
siberian
silkworm
skater
skink
skunk
sloth
slug
smelt
snail
snake
snipe
snowshoe
sole
somali
spaniel
sparrow
spider
spitz
sponge
spoonbill
squid
squirrel
squirt
starfish
stingray
stoat
stork
sturgeon
swallow
swan
swift
tahr
takin
tamarin
tang
tapir
tarantula
tarsier
termite
tern
terrier
tetra
thrush
tick
tiffany
tiger
tiglon
toad
tortoise
toucan
tropicbird
trout
tuatara
tuna
turkey
turtle
tzu
uakari
uguisu
umbrellabird
urchin
urial
vicuna
viper
vole
vulture
wallaby
walrus
warbler
warthog
wasp
weasel
whale
whippet
wildcat
wildebeest
wildfowl
wolf
wolfhound
wolverine
wombat
woodlouse
woodpecker
worm
wrasse
wren
xerinae
yak
zebra
zebu
zonkey
zorse
