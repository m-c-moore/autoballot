"""File containing the `parse_student_form()` function.

Mock data can be seen in `tests/mock_data/form/students_form`
"""

import os
from pathlib import Path
from typing import Tuple

import numpy as np
import pandas as pd

from src.params import ASYM_PREFS, CONSTANTS, SYM_PREFS

STUDENT_RENAMES = {
    "User ID": "user_id",
    "Year": "year",
    # "Fourth Year Ballot": "",
    "Syndicate ID": "syndicate_id",
    # "Which applies to you": "",
    # "Room Requirements": "",
    "Room Features": "requirements",
    "House or Staircase preference": "house",
    "Short or Long term lease": "long_lease",
    "Double Bed": "double",
    "Ensuite": "ensuite",
    "Upper floor room": "upper",
    "Room facing court (staircase only)": "faces_court",
    "Room NOT facing Lensfield Road": "not_faces_road",
    "Recently renovated kitchen": "new_kitchen",
    "Recently renovated bathroom": "new_bathroom",
    "Recently renovated room": "new_room",
    "Preferred minimum room size (m2)": "min_size",
    "Preferred maximum room size (m2)": "max_size",
    # "Anything else we should know": "",
    "Estimated Price Per Week": "est_price",
    "Acceptable overpay percentage": "price_perc_over",
    # "Acceptable Price Per Week": "",
}

SYM_COLUMNS = [
    "house",
    "long_lease",
]

ASYM_COLUMNS = [
    "double",
    "ensuite",
    "upper",
    "faces_court",
    "not_faces_road",
    "new_kitchen",
    "new_bathroom",
    "new_room",
]

STUDENT_COLUMNS = (
    [
        "student_id",
        "syndicate_id",
        "year",
        "rank",
    ]
    + SYM_COLUMNS
    + ASYM_COLUMNS
    + [
        "min_size",
        "max_size",
        "est_price",
        "price_perc_over",
    ]
)

NOT_TOGETHER_RENAMES = {
    "User ID": "user_id",
    "Can't Live With": "others_user_ids",
    "Not Together Extent": "extent",
}


def _user_id_to_student_id(user_ids: pd.Series) -> pd.Series:
    """Converts a Series of numerical ids to more human-friendly names."""
    animals = pd.read_csv("src/scripts/parse/animals.txt", header=None).squeeze("columns")
    return user_ids.replace(animals)


def _student_id_to_user_id(student_ids: pd.Series) -> pd.Series:
    """Converts a Series of human-friendly names to original user ids."""
    animals = pd.read_csv("src/scripts/parse/animals.txt", header=None).squeeze("columns")
    reverse_animals = pd.Series(data=animals.index, index=animals.values)
    return student_ids.replace(reverse_animals)


def _parse_student_form() -> Tuple[pd.DataFrame, pd.DataFrame]:
    """Create a `students.csv` file from the data provided in the form."""

    dir_path = Path(os.environ.get("INPUT_DIR", Path("data/raw_inputs")))
    in_student_df = pd.read_csv(dir_path / "students_form.csv").replace({np.nan: None})

    # create preferences csv
    student_df = in_student_df.rename(columns=STUDENT_RENAMES)
    student_df["rank"] = CONSTANTS.max_rank.value
    student_df["student_id"] = _user_id_to_student_id(student_df["user_id"])

    # convert 'word' preferences to numeric preferences
    student_df["house"] = student_df["house"].replace(
        {
            "Definitely staircase": SYM_PREFS.negative.value,
            "Probably staircase": SYM_PREFS.probably_not.value,
            "Indifferent": SYM_PREFS.indifferent.value,
            "Probably house": SYM_PREFS.probably.value,
            "Definitely house": SYM_PREFS.positive.value,
        }
    )

    student_df["long_lease"] = student_df["long_lease"].replace(
        {
            "Definitely short lease": SYM_PREFS.negative.value,
            "Probably short lease": SYM_PREFS.probably_not.value,
            "Indifferent": SYM_PREFS.indifferent.value,
            "Probably long lease": SYM_PREFS.probably.value,
            "Definitely long lease": SYM_PREFS.positive.value,
        }
    )
    student_df[ASYM_COLUMNS] = student_df[ASYM_COLUMNS].replace(
        {
            "Not at all": ASYM_PREFS.not_at_all.value,
            "Not really": ASYM_PREFS.not_really.value,
            "Indifferent": ASYM_PREFS.indifferent.value,
            "A little": ASYM_PREFS.a_little.value,
            "A lot": ASYM_PREFS.a_lot.value,
            "Definitely": ASYM_PREFS.definitely.value,
        }
    )

    # create a syndicate id for every student balloting alone
    new_syndicates = pd.Series(np.arange(student_df.shape[0]) + student_df["syndicate_id"].max())
    student_df["syndicate_id"] = student_df["syndicate_id"].fillna(new_syndicates)

    # create not together csv
    in_not_together_df = pd.read_csv(Path(dir_path, "not_together_form.csv"), dtype=str).replace({np.nan: None})

    not_together_df = in_not_together_df.rename(columns=NOT_TOGETHER_RENAMES)
    not_together_df = (
        not_together_df.assign(others_user_ids=not_together_df["others_user_ids"].str.split(","))
        .explode("others_user_ids")
        .reset_index()
    )

    not_together_df = not_together_df.replace({"Same Building": "building", "Same Block": "block"})
    not_together_df = not_together_df.astype({"user_id": int, "others_user_ids": int})

    not_together_df["student_id_1"] = _user_id_to_student_id(not_together_df["user_id"])
    not_together_df["student_id_2"] = _user_id_to_student_id(not_together_df["others_user_ids"])

    return student_df, not_together_df


if __name__ == "__main__":
    out_dir = Path("data/parsed_inputs")
    out_dir.mkdir(parents=True, exist_ok=True)

    student_df, not_together_df = _parse_student_form()

    student_df[STUDENT_COLUMNS].to_csv(out_dir / "students.csv", index=False)
    student_df[["student_id", "requirements"]].dropna().to_csv(out_dir / "requirements.csv", index=False)
    not_together_df[["student_id_1", "student_id_2", "extent"]].to_csv(out_dir / "not_together.csv", index=False)
