"""Script to identify syndicates with incompatible preferences."""

import typing

import numpy as np

from src.entities import Syndicate
from src.params import SYM_PREFS
from src.read import read_student_data


def find_syndicates_low_price_req(syndicates: typing.Iterable[Syndicate]) -> None:
    """Print syndicates with 'expensive' requirements."""
    for syndicate in syndicates:
        req_ensuite = False
        min_price = 1000.0

        for student in syndicate.students:
            min_price = min(min_price, student.prefs.acc_price)

            if student.reqs is not None:
                if student.reqs.ensuite:
                    req_ensuite = True

        if req_ensuite and min_price < 155:
            print(f"£{min_price:.2f} - syndicate id={syndicate.id}")


def _print_warning(field: str, message: str, _syndicate: Syndicate) -> None:
    print(f"'{field}' {message} - id={_syndicate.id}:")
    print("\t" + ", ".join(s.id for s in _syndicate.students))


def _find_field_disagreement(
    syndicates: typing.Iterable[Syndicate],
    field: str,
    max_sigma: typing.Optional[float] = None,
    max_diff: typing.Optional[float] = None,
) -> None:
    """Print syndicates that on their preferences.

    It considers:
        - variance over a threshold `max_sigma`;
        - max difference over a threshold `max_diff`.
    """
    for syndicate in syndicates:
        values = np.array([s.__getattribute__(field) for s in syndicate.students])

        if max_sigma is not None:
            sigma = np.var(values) ** 0.5
            if sigma >= max_sigma:
                _print_warning(field, f"sigma={sigma:.1f}", syndicate)

        if max_diff is not None:
            diff = abs(np.max(values) - np.min(values))
            if diff >= max_diff:
                _print_warning(field, f"max_diff={diff:.1f}", syndicate)


def find_syndicates_lease_disagreement(syndicates: typing.Iterable[Syndicate]) -> None:
    """Print syndicates with varied preferences for house / staircase."""
    max_diff = SYM_PREFS.positive.value - SYM_PREFS.probably_not.value
    _find_field_disagreement(syndicates, field="long_lease", max_diff=max_diff)


def find_syndicates_house_disagreement(syndicates: typing.Iterable[Syndicate]) -> None:
    """Print syndicates with varied preferences for house / staircase."""
    max_diff = SYM_PREFS.positive.value - SYM_PREFS.probably_not.value
    _find_field_disagreement(syndicates, field="house", max_sigma=0.3, max_diff=max_diff)


def find_syndicates_price_disagreement(syndicates: typing.Iterable[Syndicate]) -> None:
    """Print syndicates with varied price preferences."""
    _find_field_disagreement(syndicates, field="est_price", max_sigma=15)


def find_syndicates_acceptable_price_disagreement(syndicates: typing.Iterable[Syndicate]) -> None:
    """Print syndicates with varied acceptable price preferences."""
    max_sigma = 12

    for syndicate in syndicates:
        est_price = np.array([s.prefs.est_price for s in syndicate.students])
        acc_price = np.array([s.prefs.acc_price for s in syndicate.students])

        # find cost that is closest to the mean within the range (est_price, price_max)
        close_to_mean = np.clip(np.mean(est_price), est_price, acc_price)
        sigma = np.var(close_to_mean) ** 0.5

        if sigma >= max_sigma:
            _print_warning("acc_price", f"sigma={sigma:.1f}", syndicate)


if __name__ == "__main__":
    student_data = read_student_data()

    find_syndicates_low_price_req(student_data.syndicates)

    find_syndicates_lease_disagreement(student_data.syndicates)
    find_syndicates_house_disagreement(student_data.syndicates)
    find_syndicates_price_disagreement(student_data.syndicates)
    find_syndicates_acceptable_price_disagreement(student_data.syndicates)
