"""Script to check that preference form entries make sense."""

import typing

from src.entities import Requirements, Student
from src.entities.feats_prefs_reqs import SYMMETRIC_FEATURES
from src.params import ASYM_PREFS, SYM_PREFS
from src.read import read_student_data


def find_prefs_reqs_mismatch(students: typing.Iterable[Student]) -> None:
    """Print preferences that aren't compatible with requirements."""
    for student in students:
        if student.reqs is None:
            continue

        for field_name in Requirements.get_field_names():
            if not getattr(student.reqs, field_name):
                continue

            print_prefix = f"{student.id} : preference for {field_name} = "

            if field_name == "min_size":
                if student.prefs.min_size < student.reqs.min_size:  # type:ignore[operator] # know min_size is not None
                    print(print_prefix + f"{student.prefs.min_size} < {student.reqs.min_size}")

            elif field_name == "ground":
                if not student.prefs.upper == ASYM_PREFS.not_at_all:
                    print(print_prefix + f"{student.prefs.upper} != {ASYM_PREFS.not_at_all}")

            else:
                if field_name in SYMMETRIC_FEATURES:
                    target = SYM_PREFS.positive.value
                else:
                    target = ASYM_PREFS.definitely.value

                preference = getattr(student.prefs, field_name)
                if preference != target:
                    print(print_prefix + f"{preference} != {target}")


def find_incompatible_prefs(students: typing.Iterable[Student]) -> None:
    """Print preferences that don't make sense together."""
    for student in students:
        if student.prefs.house == SYM_PREFS.positive.value and student.prefs.long_lease == SYM_PREFS.negative.value:
            print(f"No houses are short lease - {student.id}")


if __name__ == "__main__":
    student_data = read_student_data()

    find_prefs_reqs_mismatch(student_data.students)
    find_incompatible_prefs(student_data.students)
