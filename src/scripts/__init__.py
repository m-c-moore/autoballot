"""Module containing useful scripts.

These aren't run "on the day", but are used to parse and validate the inputs prior
to running the main program.
"""
