"""File containing classes that hold the input data."""

import dataclasses
import typing

import numpy as np
import pandas as pd

from src.entities import Block, Building, Features, Preferences, Room, Student, Syndicate
from src.entities.base import Entity
from src.exceptions import DuplicateInputError, InvalidInputError
from src.params import CONSTANTS
from src.read.utils import get_entity_size_counts, get_entity_sizes


@dataclasses.dataclass(frozen=True)
class RoomData:
    """Class to hold the input room data."""

    rooms: list[Room]
    blocks: list[Block]
    buildings: list[Building]

    room_df: pd.DataFrame
    block_df: pd.DataFrame
    building_df: pd.DataFrame

    max_block_size: int
    block_sizes: list[int]
    block_size_counts: np.ndarray

    def __repr__(self) -> str:
        return f"RoomData({len(self.rooms)} rooms, {len(self.blocks)} blocks, {len(self.buildings)} buildings)"

    def __post_init__(self) -> None:
        duplicate_rooms = list(self.room_df[self.room_df.duplicated("id")]["id"])
        if len(duplicate_rooms) > 0:
            raise DuplicateInputError(f"{duplicate_rooms=}")

        duplicate_blocks = list(self.block_df[self.block_df.duplicated("id")]["id"])
        if len(duplicate_blocks) > 0:
            raise DuplicateInputError(f"{duplicate_blocks=}")

        duplicate_buildings = list(self.building_df[self.building_df.duplicated("id")]["id"])
        if len(duplicate_buildings) > 0:
            raise DuplicateInputError(f"{duplicate_buildings=}")


@dataclasses.dataclass(frozen=True)
class StudentData:
    """Class to hold the input student data."""

    students: list[Student]
    syndicates: list[Syndicate]

    student_df: pd.DataFrame
    syndicate_df: pd.DataFrame

    max_syndicate_size: int
    syndicate_sizes: list[int]
    syndicate_size_counts: np.ndarray

    def __repr__(self) -> str:
        return f"StudentData({len(self.students)} students, {len(self.syndicates)} syndicates)"

    def __post_init__(self) -> None:
        duplicate_students = list(self.student_df[self.student_df.duplicated("id")]["id"])
        if len(duplicate_students) > 0:
            raise DuplicateInputError(f"{duplicate_students=}")

        duplicate_syndicates = list(self.syndicate_df[self.syndicate_df.duplicated("id")]["id"])
        if len(duplicate_syndicates) > 0:
            raise DuplicateInputError(f"{duplicate_syndicates=}")


def generate_room_data(buildings: list[Building]) -> RoomData:
    """Return a `RoomData` instance generated from a list of `buildings`."""

    # entities
    blocks = [block for building in buildings for block in building.blocks]
    rooms = [room for block in blocks for room in block.rooms]
    rooms.sort(key=lambda room: room.index)  # needed as we use indices to generate allocations

    # dataframes
    building_fields = ["id", "num_blocks", "num_rooms", "blocks:id"]
    building_df = pd.DataFrame.from_records(_entities_to_dicts(buildings, building_fields))

    block_fields = ["id", "building.id", "is_combined", "is_split", "size", "rooms:id"]
    block_df = pd.DataFrame.from_records(_entities_to_dicts(blocks, block_fields))

    room_fields = (
        ["id", "index", "floor", "band"]
        + ["feats." + k for k in Features.get_field_names()]
        + ["block.id", "building.id"]
    )
    room_df = pd.DataFrame.from_records(_entities_to_dicts(rooms, room_fields))

    # useful properties
    max_block_size = max(block.size for block in blocks)
    if max_block_size > CONSTANTS.block_size_limit.value:
        raise InvalidInputError(f"Need to increase `CONSTANTS.block_size_limit` to at least {max_block_size=}")

    block_sizes = get_entity_sizes(blocks)
    block_size_counts = get_entity_size_counts(blocks)

    return RoomData(
        rooms=rooms,
        blocks=blocks,
        buildings=buildings,
        room_df=room_df,
        block_df=block_df,
        building_df=building_df,
        max_block_size=max_block_size,
        block_sizes=block_sizes,
        block_size_counts=block_size_counts,
    )


def generate_student_data(syndicates: list[Syndicate]) -> StudentData:
    """Return a `StudentData` instance generated from a list of `syndicates`."""

    # entities
    students = [student for syndicate in syndicates for student in syndicate.students]
    students.sort(key=lambda student: student.index)  # needed as we use indices to generate allocations

    # dataframes
    syndicate_fields = ["id", "size", "students:id"]
    syndicate_df = pd.DataFrame.from_records(_entities_to_dicts(syndicates, syndicate_fields))

    student_fields = (
        ["id", "index", "rank", "year"]
        + ["prefs." + k for k in Preferences.get_field_names()]
        + ["reqs", "syndicate.id"]
    )
    student_df = pd.DataFrame.from_records(_entities_to_dicts(students, student_fields))

    # useful properties
    max_syndicate_size = max(syndicate.size for syndicate in syndicates)
    if max_syndicate_size > CONSTANTS.block_size_limit.value:
        raise InvalidInputError(f"Need to increase `CONSTANTS.block_size_limit` to at least {max_syndicate_size=}")

    syndicate_sizes = get_entity_sizes(syndicates)
    syndicate_size_counts = get_entity_size_counts(syndicates)

    return StudentData(
        students=students,
        syndicates=syndicates,
        student_df=student_df,
        syndicate_df=syndicate_df,
        max_syndicate_size=max_syndicate_size,
        syndicate_sizes=syndicate_sizes,
        syndicate_size_counts=syndicate_size_counts,
    )


def _entities_to_dicts(
    entities: typing.Sequence[Entity], field_names: list[str]
) -> list[dict[str, typing.Union[bool, int, float, str]]]:
    """Return a list of dictionaries that can be used with `pd.DataFrame.from_records`.

    Parameters
    ----------
    entities
        List of entities whose parameters are to be extracted.
    field_names
        List of attributes to extract. Chained attributes can be specified using ".",
        e.g. `building.id` can be accessed with "building.id". List attributes
        can be summarised using ".:", e.g. "students:id" generates a comma
        separated string of student ids.
    """

    valid_types = (bool, int, float, str)
    dicts = []

    for entity in entities:
        entity_dict = {}

        for field_name in field_names:
            value: typing.Union[bool, int, float, str]

            # get properties of iterable nested field
            if ":" in field_name:
                nested_field, nested_field_property = field_name.split(":")
                fields = getattr(entity, nested_field)
                value = ", ".join(sorted(getattr(field, nested_field_property) for field in fields))

            # get property of nested field
            elif "." in field_name:
                nested_field, nested_field_property = field_name.split(".")
                value = getattr(getattr(entity, nested_field), nested_field_property)

            # get given field value
            else:
                value = getattr(entity, field_name)

            if not isinstance(value, valid_types):
                value = str(value)

            entity_dict[field_name] = value
        dicts.append(entity_dict)
    return dicts
