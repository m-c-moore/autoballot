"""File containing utility functions for the `read` module."""

import typing

import numpy as np

from src.entities import Block, Syndicate
from src.params import CONSTANTS


def get_entity_sizes(entities: typing.Union[list[Block], list[Syndicate]]) -> list[int]:
    """Return a list containing all the sizes of the given `entities`."""
    return [entity.size for entity in entities]


def get_entity_size_counts(entities: typing.Union[list[Block], list[Syndicate]]) -> np.ndarray:
    """Return an array of counts of the sizes of the given `entities`.

    The array is indexed by the size of the entity. Entities of size `0` are ignored.
    """
    sizes = get_entity_sizes(entities)
    counts = np.zeros(CONSTANTS.block_size_limit.value + 1, dtype=np.int16)

    for size in sizes:
        if size > 0:
            counts[size] += 1

    return counts
