"""File containing the `read_syndicates()` function."""

import os
import typing

import numpy as np
import pandas as pd

from src.entities import Preferences, Requirements, Student, Syndicate

from .data_types import StudentData, generate_student_data


def _extract_requirements(student_df_row: pd.Series, requirements_df: pd.DataFrame) -> typing.Optional[Requirements]:
    student_id = student_df_row["student_id"]
    try:
        requirements_row = requirements_df.loc[student_id]
    except KeyError:
        return None

    # extract list of requirements
    requirements_set: set[str] = set(requirements_row["requirements"].split(";"))
    requirements_kwargs: dict[str, typing.Union[bool, typing.Optional[int]]] = {}

    # generate kwargs for bool requirements
    for field_name in Requirements.get_bool_field_names():
        if field_name in requirements_set:
            requirements_kwargs[field_name] = True
            requirements_set.remove(field_name)
        else:
            requirements_kwargs[field_name] = False

    # generate kwargs for requirements
    for field_name in ["min_size"]:
        for requirement in requirements_set:
            if field_name in requirement:
                requirements_kwargs[field_name] = int(requirement.split("=")[1])
                requirements_set.remove(requirement)
                break
        else:
            requirements_kwargs[field_name] = None

    assert len(requirements_set) == 0, f"Unrecognised / duplicate requirements {requirements_set}"
    return Requirements(**requirements_kwargs)  # type: ignore[arg-type]


def read_student_data() -> StudentData:
    """Return a `StudentData` instance generated from the input data."""

    dir_path = os.environ.get("INPUT_DIR", os.path.join("data", "parsed_inputs"))
    df = pd.read_csv(os.path.join(dir_path, "students.csv")).replace({np.nan: None})
    req_df = pd.read_csv(os.path.join(dir_path, "requirements.csv")).set_index("student_id").replace({np.nan: None})

    syndicate_by_id: dict[str, Syndicate] = {}

    for index, row in df.iterrows():
        preferences = Preferences(**{field_name: row[field_name] for field_name in Preferences.get_field_names()})
        requirements = _extract_requirements(row, req_df)

        student = Student(
            row["student_id"],
            index=index,
            rank=row["rank"],
            year=row["year"],
            prefs=preferences,
            reqs=requirements,
        )

        syndicate_id = str(row["syndicate_id"])
        try:
            syndicate_by_id[syndicate_id].add_child(student)
        except KeyError:
            syndicate = Syndicate(syndicate_id)
            syndicate.add_child(student)
            syndicate_by_id[syndicate_id] = syndicate

    # finalise syndicates
    syndicates = []
    for syndicate in syndicate_by_id.values():
        syndicate.finalise()
        syndicates.append(syndicate)

    return generate_student_data(syndicates)
