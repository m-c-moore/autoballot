"""Module containing functions to read and parse the input data."""

from .data_types import RoomData, StudentData
from .read_rooms import read_room_data
from .read_students import read_student_data

__all__ = ["read_room_data", "read_student_data", "RoomData", "StudentData"]
