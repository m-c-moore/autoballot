"""File containing the `read_room_data()` function."""

import datetime
import os
from collections import defaultdict

import numpy as np
import pandas as pd

from src.entities import Block, Building, Features, Room

from .data_types import RoomData, generate_room_data


def _check_and_parse_input(df: pd.DataFrame) -> None:
    assert set(df["floor"]).issubset({1, 2, 3, 4})
    df["upper"] = df["floor"] >= 2

    assert set(df["contract_length"]).issubset({30, 38})
    df["long_lease"] = df["contract_length"] == 38

    # map renovation values to decimal (0 = oldest, 1 = newest)
    current_year = datetime.datetime.now().year
    age_range = 22  # anything older than 22 years is treated as 22 years old

    def normalise_year(year: float) -> float:
        assert year <= current_year
        return max(0.0, 1 - (current_year - year) / age_range)

    df["new_bathroom"] = df["bathroom_ren"].apply(normalise_year)
    df["new_kitchen"] = df["kitchen_ren"].apply(normalise_year)
    df["new_room"] = df["room_ren"].apply(normalise_year)


def read_room_data() -> RoomData:
    """Return a `RoomData` instance generated from the input data."""
    dir_path = os.environ.get("INPUT_DIR", os.path.join("data", "parsed_inputs"))
    df = pd.read_csv(os.path.join(dir_path, "rooms.csv")).replace({np.nan: None})
    _check_and_parse_input(df)

    # initialise blocks and buildings dictionaries
    block_by_building_id: defaultdict[str, dict[str, Block]] = defaultdict(lambda: {})
    building_by_id: dict[str, Building] = {}

    for index, row in df.iterrows():
        features = Features(**{field_name: row[field_name] for field_name in Features.get_field_names()})
        room = Room(row["room_id"], index=index, floor=row["floor"], band=row["band"], feats=features)

        block_id = row["block_id"]
        building_id = row["building_id"]

        try:
            block_by_building_id[building_id][block_id].add_child(room)
        except KeyError:
            block = Block(block_id)
            block.add_child(room)
            block_by_building_id[building_id][block_id] = block

            try:
                building_by_id[building_id].add_child(block)
            except KeyError:
                building = Building(building_id)
                building.add_child(block)
                building_by_id[building_id] = building

    # finalise buildings and blocks
    buildings = []
    for building in building_by_id.values():
        building.finalise()
        buildings.append(building)

    return generate_room_data(buildings)
