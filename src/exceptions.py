"""File containing custom exception classes.

Note that in performance critical functions asserts are used instead of checks with
exceptions - this is to make it easy to switch off these checks (using the "-O" flag)
since, in theory, they should never raise errors.
"""


class _CustomError(Exception):
    pass


class ImpossibleBallotError(_CustomError):
    """Cannot run the ballot with given inputs."""

    pass


class InvalidInputError(_CustomError):
    """Input data contains invalid values."""

    pass


class DuplicateInputError(_CustomError):
    """Input data contains duplicates."""

    pass


class MatchError(_CustomError):
    """Cannot perform match."""

    pass


class BlockConfigError(_CustomError):
    """Cannot generate block configuration."""

    pass


class ImpossibleAssignmentError(_CustomError):
    """Assignment with finite cost not possible."""

    pass


class NoMoreSplitsError(_CustomError):
    """No more possible block splits are available."""

    pass
