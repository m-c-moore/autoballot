"""File containing base classes for entities."""

from __future__ import annotations

import typing
from abc import ABC


class Entity(ABC):
    """Abstract base class from which all entities are extended."""

    def __init__(self, id_: str) -> None:
        """Initialise an entity with a unique `id`."""
        self._id = id_  # TODO assert unique

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({self.id})"

    @property
    def id(self) -> str:
        """The entity's unique id."""
        return self._id

    def __eq__(self, other: typing.Any) -> bool:
        return type(self) == type(other) and self.id == other.id


T_Parent = typing.TypeVar("T_Parent", bound="ParentEntity")


class ChildEntity(Entity, ABC, typing.Generic[T_Parent]):
    """Abstract base class from which all child entities are extended.

    Child entities are those that are associated with exactly one parent entity.
    """

    def __init__(self, id_: str):
        """Initialise a child entity with a unique `id`."""

        super().__init__(id_)
        self.__parent: typing.Optional[T_Parent] = None
        self._repr_props: list[str] = []

    def __repr__(self) -> str:
        return super().__repr__() + "{" + ", ".join(f"{p}={getattr(self, p)}" for p in self._repr_props) + "}"

    @property
    def _parent(self) -> T_Parent:
        assert self.__parent is not None
        return self.__parent

    @_parent.setter
    def _parent(self, parent: T_Parent) -> None:
        assert self.__parent is None, f"'{self.id}' already in '{self.__parent}'"
        self.__parent = parent


T_Child = typing.TypeVar("T_Child", bound=ChildEntity)


class ParentEntity(Entity, ABC, typing.Generic[T_Child]):
    """Abstract base class from which all parent entities are extended.

    Parent entities are those that are associated with one more child entities.
    """

    def __init__(self, id_: str):
        """Initialise a parent entity with a unique `id`."""

        super().__init__(id_)
        self.__children: list[T_Child] = []
        self._final: bool = False

    def __repr__(self) -> str:
        return super().__repr__() + "[" + ", ".join(c.id for c in self._children) + "]"

    @property
    def _children(self) -> list[T_Child]:
        return self.__children

    @_children.setter
    def _children(self, children: list[T_Child]) -> None:
        assert len(self.__children) == 0
        assert not self._final
        self.__children = children

    @property
    def size(self) -> int:
        """The number of children associated with this entity."""
        assert self._final
        return len(self._children)

    def add_child(self, child: T_Child) -> None:
        """Add a child to the parent entity."""
        assert not self._final
        child._parent = self
        self.__children.append(child)

    def finalise(self) -> None:
        """Mark the parent as 'finalised', preventing more children from being added."""
        self._final = True
