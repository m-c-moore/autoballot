"""File containing the `Student` and `Syndicate` classes."""

from __future__ import annotations

import typing

from src.exceptions import InvalidInputError
from src.params import ASYM_PREFS, CONSTANTS, SYM_PREFS

from .base import ChildEntity, ParentEntity
from .feats_prefs_reqs import SYMMETRIC_FEATURES, Preferences, Requirements


class Student(ChildEntity["Syndicate"]):
    """Class representing a student."""

    def __init__(
        self,
        id_: str,
        *,
        index: int,  # TODO assert unique
        rank: float,  # TODO limit to range [0,1]
        year: int,
        prefs: Preferences,
        reqs: typing.Optional[Requirements],
    ):
        """Initialise a `Student` instance.

        Parameters
        ----------
        id_
            The student's unique id.
        index
            The student's index in the input list.
        rank
            The student's effective ballot rank. Can be used to weight their preferences.
        year
            The student's year. `1` corresponds to first year going into second year.
        prefs
            The student's room preferences.
        reqs
            The student's room requirements if applicable, otherwise `None`.
        """

        super().__init__(id_)

        if not (1 <= rank <= CONSTANTS.max_rank.value):
            raise InvalidInputError(f"{rank=} not in range")

        if year not in {1, 2, 3, 4}:
            raise InvalidInputError(f"{year=} not in range")

        self._index = index
        self._rank = rank
        self._year = year
        self._prefs = prefs
        self._reqs = reqs

        self._repr_props += ["index", "rank", "year"]

    @property
    def index(self) -> int:
        """The student's index in the input list."""
        return self._index

    @property
    def rank(self) -> float:
        """The student's effective ballot rank. Can be used to weight their preferences."""
        return self._rank

    @property
    def year(self) -> int:
        """The student's year. `1` corresponds to first year going into second year."""
        return self._year

    @property
    def prefs(self) -> Preferences:
        """The student's room preferences."""
        return self._prefs

    @property
    def reqs(self) -> typing.Optional[Requirements]:
        """The student's room requirements if applicable, otherwise `None`."""
        return self._reqs

    @property
    def syndicate(self) -> Syndicate:
        """The student's syndicate."""
        return self._parent

    def has_preference(self, preference_name: str) -> bool:
        """Return `True` if the student has the named preference."""
        tol = 0.001  # for float errors
        if preference_name == "staircase":
            return getattr(self.prefs, "house") <= SYM_PREFS.positive.value + tol

        preference_value = getattr(self.prefs, preference_name) + tol

        if preference_value in SYMMETRIC_FEATURES:
            return preference_value >= SYM_PREFS.positive.value

        return preference_value >= ASYM_PREFS.definitely.value


class Syndicate(ParentEntity[Student]):
    """Class representing a syndicate (i.e. a group of students)."""

    @property
    def students(self) -> list[Student]:
        """The students in the syndicate."""
        return self._children
