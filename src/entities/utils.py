"""File containing the utility functions for the `entities` module."""

from src.params import CONSTANTS, SYM_PREFS
from src.read import StudentData

from .feats_prefs_reqs import Preferences
from .student import Student, Syndicate

_NUM_PLACEHOLDERS = 0


def add_placeholder_student(student_data: StudentData) -> Student:
    """Add a placeholder `Student` instance (in a syndicate of 1) to the program.

    Adding a few placeholder students may help the `shuffle` module to generate more
    valid layouts if there are more rooms than students. This student will be
    indifferent about everything (including price), meaning they will have the same
    match cost in every room thus won't affect the allocations themselves (although
    the solution stats will change).
    """
    global _NUM_PLACEHOLDERS

    _NUM_PLACEHOLDERS += 1

    preferences = Preferences(
        **{field_name: SYM_PREFS.indifferent.value for field_name in Preferences.get_bool_field_names()},
        min_size=CONSTANTS.min_room_size.value,
        max_size=CONSTANTS.max_room_size.value,
        est_price=CONSTANTS.max_room_price.value,
        price_perc_over=CONSTANTS.max_price_perc_over.value,
    )

    student = Student(
        f"_student_{_NUM_PLACEHOLDERS}",
        index=len(student_data.students),
        rank=CONSTANTS.max_rank.value,
        year=1,
        prefs=preferences,
        reqs=None,
    )
    student_data.students.append(student)

    syndicate = Syndicate(f"_syndicate_{_NUM_PLACEHOLDERS}")
    syndicate.add_child(student)
    syndicate.finalise()
    student_data.syndicates.append(syndicate)

    return student
