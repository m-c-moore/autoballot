"""File containing classes representing room features, preferences and requirements."""

import abc
import dataclasses
import typing

import numpy as np

from src.exceptions import InvalidInputError
from src.params import CONSTANTS, VALID_ASYM_PREF_VALUES, VALID_SYM_PREF_VALUES, VECTORS

SYMMETRIC_FEATURES = {"house", "long_lease"}
NEW_FEATURES = {"new_bathroom", "new_kitchen", "new_room"}

T = typing.TypeVar("T", bool, float)


@dataclasses.dataclass(frozen=True)
class _BaseBoolFeatures(abc.ABC, typing.Generic[T]):
    """Base class from which other feature classes are extended.

    This class only defines boolean features, i.e., those that either do exist in a
    room or do not (such as double beds, ensuites etc.). Note that preferences for
    these features will all still be expressed as floats.
    """

    double: T
    ensuite: T
    upper: T
    faces_court: T
    not_faces_road: T

    house: T
    long_lease: T

    def _field_to_bool(self, field_name: str) -> None:
        """Convert the given field value from `1` or `0` to `True` or `False`."""
        field_value = getattr(self, field_name)

        if not (isinstance(field_value, bool) or field_value in {0, 1}):
            raise InvalidInputError(f"Expected boolean value for '{field_name}', got '{field_value}'")

        object.__setattr__(self, field_name, bool(field_value))  # avoid frozen dataclass error

    @classmethod
    def get_field_names(cls) -> list[str]:
        """Return the names of all the public fields defined in the dataclass."""
        return [field.name for field in dataclasses.fields(cls) if not field.name.startswith("_")]

    @classmethod
    def get_bool_field_names(cls) -> list[str]:
        """Return the names of all the public fields associated with boolean features.

        This should be manually overridden in subclasses where necessary.
        """
        return cls.get_field_names()


@dataclasses.dataclass(frozen=True)
class BoolFeatures(_BaseBoolFeatures[T]):
    """Base class from which room `Features` and student `Preferences` are extended.

    This class adds features describing renovations. Whether a something was "recently
    renovated" is true or false, although this is represented as a float to indicate
    how recently it was renovated.

     The exception to this are
    features describing renovations - for example,

    Note that preferences for these features will all still be expressed as floats.
    """

    new_kitchen: float
    new_bathroom: float
    new_room: float

    _vector: np.ndarray = dataclasses.field(
        default_factory=np.zeros(10, dtype=np.float32).copy,
        init=False,
        repr=False,
    )

    def __post_init__(self) -> None:
        self._vector[:] = [
            self.double,
            self.ensuite,
            self.upper,
            self.faces_court,
            self.not_faces_road,
            self.house,
            self.long_lease,
            self.new_kitchen,
            self.new_bathroom,
            self.new_room,
        ]

    @property
    def vector(self) -> np.ndarray:
        """A vector representation of the features / preferences."""
        return self._vector


_BOOL_FEATURE_WEIGHTS = BoolFeatures(
    **{field_name: getattr(VECTORS, field_name).value for field_name in BoolFeatures.get_field_names()},
)


@dataclasses.dataclass(frozen=True)
class Features(BoolFeatures[bool]):
    """Class representing room features."""

    # additional numeric features
    size: float  # m^2
    price: float  # £ per week

    def __post_init__(self) -> None:
        super().__post_init__()

        for field_name in self.get_bool_field_names():
            if field_name in NEW_FEATURES:
                if not (0 <= getattr(self, field_name) <= 1):
                    raise InvalidInputError(f"{field_name}={getattr(self, field_name)} out of range")
            else:
                self._field_to_bool(field_name)

        if not (CONSTANTS.min_room_size.value <= self.size <= CONSTANTS.max_room_size.value):
            raise InvalidInputError(f"{self.size=} out of range")

        if not (CONSTANTS.min_room_price.value <= self.price <= CONSTANTS.max_room_price.value):
            raise InvalidInputError(f"{self.price=} out of range")

    @property
    def vector(self) -> np.ndarray:
        """A vector representation of the features."""
        # map [0,1] -> [-1,1] then scale by weights
        return np.multiply(super().vector * 2 - 1, _BOOL_FEATURE_WEIGHTS.vector)

    @classmethod
    def get_bool_field_names(cls) -> list[str]:
        """Return the names of all the public fields associated with boolean features."""
        return BoolFeatures.get_bool_field_names()


@dataclasses.dataclass(frozen=True)
class Preferences(BoolFeatures[float]):
    """Class representing student preferences for certain room features."""

    # additional preferences for numeric features
    min_size: float  # m^2
    max_size: float  # m^2
    est_price: float  # £ per week
    price_perc_over: float  # %

    def __post_init__(self) -> None:
        super().__post_init__()

        for field_name in self.get_bool_field_names():
            valid_values = VALID_SYM_PREF_VALUES if field_name in SYMMETRIC_FEATURES else VALID_ASYM_PREF_VALUES

            if getattr(self, field_name) not in valid_values:
                raise InvalidInputError(f"'{field_name}'={getattr(self, field_name)} not in {valid_values}")

        if not (CONSTANTS.min_room_size.value <= self.min_size <= CONSTANTS.max_room_size.value):
            raise InvalidInputError(f"{self.min_size=} out of range")

        if not (CONSTANTS.min_room_size.value <= self.max_size <= CONSTANTS.max_room_size.value):
            raise InvalidInputError(f"{self.max_size=} out of range")

        if self.min_size > self.max_size:
            raise InvalidInputError(f"{self.min_size=} must be less than {self.max_size=}")

        if not (CONSTANTS.min_room_price.value <= self.est_price <= CONSTANTS.max_room_price.value):
            raise InvalidInputError(f"{self.est_price=} out of range")

        if not (CONSTANTS.min_price_perc_over.value <= self.price_perc_over <= CONSTANTS.max_price_perc_over.value):
            raise InvalidInputError(f"{self.price_perc_over=} out of range")

    @property
    def acc_price(self) -> float:
        """The student's maximum acceptable price. Equal to `est_price * (1 + price_perc_over/100)`."""
        return self.est_price * (1 + self.price_perc_over / 100)

    @classmethod
    def get_bool_field_names(cls) -> list[str]:
        """Return the names of all the public fields associated with boolean features."""
        return BoolFeatures.get_bool_field_names()


@dataclasses.dataclass(frozen=True)
class Requirements(_BaseBoolFeatures[bool]):
    """Class representing student requirements for certain room features."""

    ground: bool
    min_size: typing.Optional[float]  # m^2

    def __post_init__(self) -> None:
        for field_name in self.get_bool_field_names():
            self._field_to_bool(field_name)

        if self.upper and self.ground:
            raise InvalidInputError("Cannot require upper floor room and ground floor room")

        if self.min_size is not None:
            if not (CONSTANTS.min_room_size.value <= self.min_size <= CONSTANTS.max_room_size.value):
                raise InvalidInputError(f"{self.min_size=} out of range")

    def __repr__(self) -> str:
        requirements = [field for field in self.get_bool_field_names() if getattr(self, field)]

        if self.min_size is not None:
            requirements.append(f">={self.min_size}m")

        return f"Req({','.join(requirements)})"

    def is_missing_requirements(self, features: Features) -> bool:
        """Return `True` if the given set of room `features` is missing requirements."""
        for field_name in _BaseBoolFeatures.get_field_names():
            if getattr(self, field_name) and not getattr(features, field_name):
                return True

        if self.ground and features.upper:
            return True

        if self.min_size is not None and features.size < self.min_size:
            return True

        return False

    @classmethod
    def get_bool_field_names(cls) -> list[str]:
        """Return the names of all the public fields associated with boolean features."""
        return _BaseBoolFeatures.get_bool_field_names() + ["ground"]
