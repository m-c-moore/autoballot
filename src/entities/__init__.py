"""Module containing classes to define the main objects in the program.

This includes: students; syndicates of students; rooms; isolated blocks of rooms; and
buildings containing one or more blocks.
"""

from .feats_prefs_reqs import BoolFeatures, Features, Preferences, Requirements
from .room import Block, Building, CBlock, Room, SBlock, SCBlock
from .student import Student, Syndicate

__all__ = [
    "Block",
    "CBlock",
    "SBlock",
    "SCBlock",
    "Building",
    "Room",
    "BoolFeatures",
    "Preferences",
    "Features",
    "Requirements",
    "Student",
    "Syndicate",
]
