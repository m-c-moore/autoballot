<img src="img/jcr-logo.png" title="Downing JCR Logo" width=100 align="right">

# Downing JCR Room Allocation Program

[![Notion](https://img.shields.io/badge/Info%20%2F%20FAQs-Notion-8E1664?logo=Notion)](https://www.notion.so/Downing-JCR-Room-Ballot-8cce8287fdd84ef98861f69e9ec01908) | [![pdoc3](https://img.shields.io/badge/Docs-pdoc3-blue)](https://m-c-moore.gitlab.io/autoballot/src/) | [![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](LICENSE) | [![pipelines](https://gitlab.com/m-c-moore/autoballot/badges/master/pipeline.svg)](https://gitlab.com/m-c-moore/autoballot/-/commits/master)

A program that aims to optimally assign students to rooms by matching their preferences to room facilities. It also ensures that friend groups are not separated and no one is allocated a room that does not match medical / religious requirements or is too expensive.

- [Why was it developed?](#why-was-it-developed)
- [How does it work?](#how-does-it-work)
- [Installation / usage](#installation--usage)
- [Contributing](#contributing)
- [Contributors](#contributors)

## Why was it developed?

This project was originally proposed to help Downing College re-allocate rooms to its undergraduates in a COVID secure way in the summer of 2020. In contrast to alternative allocation schemes, this program was able to ensure that:

- Students with medical or religious requirements could 'ballot' with a group of friends (previously they were assigned a room separately);
- Students were not forced into rooms that they could not afford;
- Balloting groups of friends were not separated, regardless of their size (up to 8 people);
- Some students were allowed to remain in their previously allocated rooms;
- Fourth year students were included in the process.

The first three points significantly improved upon the original ballot process, in which students were put in a random order and selected rooms sequentially. The JCR therefore voted to use this system going forward.

## How does it work?

Broadly speaking, the program's operation can be broken down into four stages:

1. **Data processing** - inputs are read from CSVs and objects representing students, rooms, blocks etc. are generated. There are some scripts to perform validation and sanity checks on this data.
1. **Block configuration generation** - the available blocks of rooms are (semi) randomly selected, combined and split to match the given syndicate sizes. This produces an independent optimisation problem for each block / syndicate size.
    - The randomness of this stage can be adjusted to trade off the success rate of generating solutions with their variability.
1. **Optimal assignment** - for each valid block configuration, syndicates are optimally assigned to blocks (and students to rooms within that) to minimise the total assignment cost.
    - The student-room assignment costs are calculated by comparing the student's preferences and requirements to the room features / facilities.
    - Ensuring that students are allocated rooms they can afford is one of the primary goals of this program, thus the student-room match cost grows exponentially with the difference between the room's price and the student's desired price.
    - We avoid allocating rooms that don't meet students' specific medical or religious requirements by assigning infinite costs to those matches.
1. **Solution analysis** - each valid solution (i.e. those with finite total costs) is evaluated using a number of metrics in addition to the overall cost, such as statistics about how much students are overpaying by. To ensure fairness, aggregated statistics are considered from both an average-case and worst-case point of view.

> Much more information about how the program works can be found in the [user guide](user_guide.ipynb), [Notion](https://www.notion.so/Downing-JCR-Room-Ballot-8cce8287fdd84ef98861f69e9ec01908) and [documentation](https://m-c-moore.gitlab.io/autoballot/src/) pages.

## Installation / usage

Please refer to [user_guide.ipynb](user_guide.ipynb) for detailed instructions about how to install and run the program.

If you are interested in using this program for your own college or for a different group-based allocation problem, our [license](LICENSE) permits this. While we have tried to be fairly general in our approach, the project was developed with Downing's specific situation in mind, so some parts may not be easily transferable. Please [get in touch](https://gitlab.com/m-c-moore) if you want to find out how it could work for you or for help setting it up.

## Contributing

Whether it be new features, performance improvements, bug fixes, documentation or tests, we would love to have your contributions. Please refer to [CONTRIBUTING.md](CONTRIBUTING.md) for more information.

## Contributors

- [Matt Moore](https://gitlab.com/m-c-moore): [v1.0.0](https://gitlab.com/m-c-moore/autoballot/-/releases/v1.0.0) - [v2.0.0](https://gitlab.com/m-c-moore/autoballot/-/releases/v2.0.0)
- [Toby Boyne](https://github.com/TobyBoyne): [v1.1.0](https://gitlab.com/m-c-moore/autoballot/-/releases/v1.1.0) - [v2.0.0](https://gitlab.com/m-c-moore/autoballot/-/releases/v2.0.0)
- [Tommy Rochussen](https://github.com/Sheev13): [v1.1.0](https://gitlab.com/m-c-moore/autoballot/-/releases/v1.1.0)
- [Constance Kraay](https://gitlab.com/ckraay): [v1.1.0](https://gitlab.com/m-c-moore/autoballot/-/releases/v1.1.0)

Special thanks to [Lawrence Brown](https://github.com/albino) for creating the [survey website](https://github.com/dowjcr/roomsurvey/) and to Mateo Austin and Bry Sheridan for all their support.
